<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="bGTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="bText" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="171" name="Fraes_2mm" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="xfer" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="#afb-Boards">
<packages>
<package name="INA219-MODULE">
<wire x1="-12.319" y1="-8.636" x2="-12.319" y2="8.524" width="0.127" layer="21"/>
<wire x1="-12.319" y1="8.524" x2="-9.779" y2="11.064" width="0.127" layer="21" curve="-90"/>
<wire x1="-9.779" y1="11.064" x2="10.541" y2="11.064" width="0.127" layer="21"/>
<wire x1="10.541" y1="11.064" x2="13.081" y2="8.524" width="0.127" layer="21" curve="-90"/>
<wire x1="13.081" y1="8.524" x2="13.081" y2="-8.636" width="0.127" layer="21"/>
<wire x1="13.081" y1="-8.636" x2="10.541" y2="-11.176" width="0.127" layer="21" curve="-90"/>
<wire x1="10.541" y1="-11.176" x2="-9.779" y2="-11.176" width="0.127" layer="21"/>
<wire x1="-9.779" y1="-11.176" x2="-12.319" y2="-8.636" width="0.127" layer="21" curve="-90"/>
<rectangle x1="-2.159" y1="-8.636" x2="-2.032" y2="-8.128" layer="42"/>
<rectangle x1="-4.064" y1="-8.636" x2="-3.937" y2="-8.128" layer="42"/>
<pad name="1" x="-5.588" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="2" x="-3.048" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="3" x="-0.508" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="4" x="2.032" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="5" x="4.572" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="6" x="7.112" y="-8.382" drill="1" diameter="1.9304"/>
<pad name="P$8" x="-9.779" y="-8.636" drill="2.8"/>
<pad name="P$9" x="10.541" y="-8.636" drill="2.8"/>
<pad name="P$10" x="-9.779" y="8.509" drill="2.8"/>
<pad name="P$11" x="10.541" y="8.509" drill="2.8"/>
<wire x1="-3.302" y1="10.795" x2="4.826" y2="10.795" width="0.127" layer="25"/>
<wire x1="4.826" y1="10.795" x2="4.826" y2="5.08" width="0.127" layer="25"/>
<wire x1="4.826" y1="5.08" x2="-3.302" y2="5.08" width="0.127" layer="25"/>
<wire x1="-3.302" y1="5.08" x2="-3.302" y2="10.795" width="0.127" layer="25"/>
<text x="-11.43" y="-12.7" size="1.27" layer="27">&gt;VALUE</text>
<text x="-11.43" y="12.7" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LOLIND32">
<description>Wemos Lolin D32 ESP32 development board with battery port.
VP and VN are Not Connected pins</description>
<wire x1="-12.7" y1="-36.78" x2="13.03" y2="-36.78" width="0.127" layer="21"/>
<wire x1="13.03" y1="-36.78" x2="13.03" y2="28.22" width="0.127" layer="21"/>
<wire x1="13.03" y1="28.22" x2="-12.7" y2="28.22" width="0.127" layer="21"/>
<wire x1="-12.7" y1="28.22" x2="-12.7" y2="-19.304" width="0.127" layer="21"/>
<text x="-12.7" y="30.22" size="1.27" layer="25">&gt;NAME</text>
<text x="-12.7" y="-38.78" size="1.27" layer="27">&gt;VALUE</text>
<pad name="32" x="11.58" y="21.59" drill="1" diameter="1.524" shape="octagon"/>
<pad name="31" x="11.58" y="19.05" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="30" x="11.58" y="16.51" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="29" x="11.58" y="13.97" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="28" x="11.58" y="11.43" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="27" x="11.58" y="8.89" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="26" x="11.58" y="6.35" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="25" x="11.58" y="3.81" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="24" x="11.58" y="1.27" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="23" x="11.58" y="-1.27" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="22" x="11.58" y="-3.81" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="21" x="11.58" y="-6.35" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="20" x="11.58" y="-8.89" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="19" x="11.58" y="-11.43" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="18" x="11.58" y="-13.97" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="17" x="11.58" y="-16.51" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="16" x="-11.43" y="-16.51" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="15" x="-11.43" y="-13.97" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="14" x="-11.43" y="-11.43" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="13" x="-11.43" y="-8.89" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="12" x="-11.43" y="-6.35" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="11" x="-11.43" y="-3.81" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="10" x="-11.43" y="-1.27" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="9" x="-11.43" y="1.27" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="8" x="-11.43" y="3.81" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="7" x="-11.43" y="6.35" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="6" x="-11.43" y="8.89" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="5" x="-11.43" y="11.43" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="4" x="-11.43" y="13.97" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="3" x="-11.43" y="16.51" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="2" x="-11.43" y="19.05" drill="1" diameter="1.5748" shape="octagon"/>
<pad name="1" x="-11.43" y="21.59" drill="1" diameter="1.5748" shape="octagon"/>
<wire x1="-12.7" y1="-25.781" x2="-12.7" y2="-36.78" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-36.703" x2="-3.429" y2="-30.861" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-30.861" x2="4.064" y2="-30.861" width="0.127" layer="21"/>
<wire x1="4.064" y1="-30.861" x2="4.064" y2="-36.703" width="0.127" layer="21"/>
<wire x1="4.064" y1="-36.703" x2="-3.429" y2="-36.703" width="0.127" layer="21"/>
<wire x1="12.954" y1="-19.431" x2="7.112" y2="-19.431" width="0.127" layer="21"/>
<wire x1="7.112" y1="-19.431" x2="7.112" y2="-30.226" width="0.127" layer="21"/>
<wire x1="7.112" y1="-30.226" x2="12.954" y2="-30.226" width="0.127" layer="21"/>
<wire x1="12.954" y1="-30.226" x2="12.954" y2="-19.431" width="0.127" layer="21"/>
<wire x1="-12.573" y1="-28.067" x2="-5.715" y2="-28.067" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-28.067" x2="-5.715" y2="-29.972" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-30.988" x2="-5.715" y2="-33.528" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-34.544" x2="-5.715" y2="-36.703" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-36.703" x2="-12.573" y2="-36.703" width="0.127" layer="21"/>
<wire x1="-12.573" y1="-36.703" x2="-12.573" y2="-28.067" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-29.972" x2="-10.414" y2="-29.972" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-29.972" x2="-10.414" y2="-30.988" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-30.988" x2="-5.715" y2="-30.988" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-30.988" x2="-5.715" y2="-29.972" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-33.528" x2="-5.715" y2="-33.528" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-33.528" x2="-5.715" y2="-34.544" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-34.544" x2="-10.414" y2="-34.544" width="0.127" layer="21"/>
<wire x1="-10.414" y1="-34.544" x2="-10.414" y2="-33.528" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-19.304" x2="-8.89" y2="-19.304" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-19.304" x2="-8.89" y2="-25.781" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-25.781" x2="-12.7" y2="-25.781" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-25.781" x2="-12.7" y2="-19.304" width="0.127" layer="21"/>
<text x="-1.27" y="-34.163" size="1.27" layer="21">USB</text>
<wire x1="6.096" y1="-32.893" x2="12.7" y2="-32.893" width="0.127" layer="21"/>
<wire x1="12.7" y1="-32.893" x2="12.7" y2="-36.195" width="0.127" layer="21"/>
<wire x1="12.7" y1="-36.195" x2="6.096" y2="-36.195" width="0.127" layer="21"/>
<wire x1="6.096" y1="-36.195" x2="6.096" y2="-32.893" width="0.127" layer="21"/>
<circle x="9.525" y="-34.544" radius="1.17088125" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-2.54" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.89" y2="20.32" width="0.127" layer="21"/>
<wire x1="8.89" y1="20.32" x2="8.89" y2="27.94" width="0.127" layer="21"/>
<wire x1="8.89" y1="27.94" x2="-8.89" y2="27.94" width="0.127" layer="21"/>
<wire x1="-8.89" y1="27.94" x2="-8.89" y2="22.86" width="0.127" layer="21"/>
<wire x1="-8.89" y1="22.86" x2="-8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-8.89" y1="22.86" x2="3.81" y2="22.86" width="0.127" layer="21"/>
<wire x1="3.81" y1="22.86" x2="3.81" y2="20.32" width="0.127" layer="21"/>
<wire x1="3.81" y1="20.32" x2="8.89" y2="20.32" width="0.127" layer="21"/>
<wire x1="-7.62" y1="24.13" x2="-7.62" y2="27.305" width="0.127" layer="21"/>
<wire x1="-7.62" y1="27.305" x2="-5.715" y2="27.305" width="0.127" layer="21"/>
<wire x1="-5.715" y1="27.305" x2="-5.715" y2="24.13" width="0.127" layer="21"/>
<wire x1="-5.715" y1="24.13" x2="-3.81" y2="24.13" width="0.127" layer="21"/>
<wire x1="-3.81" y1="24.13" x2="-3.81" y2="27.305" width="0.127" layer="21"/>
<wire x1="-3.81" y1="27.305" x2="-1.905" y2="27.305" width="0.127" layer="21"/>
<wire x1="-1.905" y1="27.305" x2="-1.905" y2="24.13" width="0.127" layer="21"/>
<wire x1="-1.905" y1="24.13" x2="0" y2="24.13" width="0.127" layer="21"/>
<wire x1="0" y1="24.13" x2="0" y2="27.305" width="0.127" layer="21"/>
<wire x1="0" y1="27.305" x2="1.905" y2="27.305" width="0.127" layer="21"/>
<wire x1="1.905" y1="27.305" x2="1.905" y2="24.13" width="0.127" layer="21"/>
<wire x1="1.905" y1="24.13" x2="3.81" y2="24.13" width="0.127" layer="21"/>
<wire x1="3.81" y1="24.13" x2="3.81" y2="27.305" width="0.127" layer="21"/>
<wire x1="3.81" y1="27.305" x2="5.715" y2="27.305" width="0.127" layer="21"/>
<wire x1="5.715" y1="22.86" x2="5.715" y2="27.305" width="0.127" layer="21"/>
<wire x1="5.715" y1="27.305" x2="7.62" y2="27.305" width="0.127" layer="21"/>
<wire x1="7.62" y1="27.305" x2="7.62" y2="22.86" width="0.127" layer="21"/>
</package>
<package name="BATT_18560_HOLDER_DIP">
<description>Battery 18650 Holder</description>
<wire x1="-10.5" y1="-38.75" x2="-10.5" y2="38.75" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-38.75" x2="10.5" y2="-38.75" width="0.127" layer="21"/>
<pad name="N" x="0" y="-36" drill="1.6" diameter="2.54" shape="square"/>
<pad name="P" x="0" y="36" drill="1.6" diameter="2.54"/>
<wire x1="-10.5" y1="38.75" x2="10.5" y2="38.75" width="0.127" layer="21"/>
<wire x1="10.5" y1="38.75" x2="10.5" y2="-38.75" width="0.127" layer="21"/>
<text x="0" y="30.97" size="3.81" layer="21" align="center">+</text>
<text x="0" y="-30.97" size="5.08" layer="21" align="center">-</text>
<text x="-10.5" y="39.25" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.51" y="-37.17" size="1.4224" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="INA219-MODULE">
<pin name="VCC" x="-12.7" y="7.62" length="middle"/>
<pin name="GND" x="-12.7" y="5.08" length="middle"/>
<pin name="SCL" x="-12.7" y="2.54" length="middle"/>
<pin name="SDA" x="-12.7" y="0" length="middle"/>
<pin name="VIN-" x="-12.7" y="-2.54" length="middle"/>
<pin name="VIN+" x="-12.7" y="-5.08" length="middle"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<text x="2.54" y="7.62" size="1.27" layer="94">INA219</text>
<text x="-7.62" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LOLIND32">
<description>Wemos Lolin D32 
ESP32 development board with battery port.
VP and VN are Not Connected pins</description>
<wire x1="-15.1918" y1="21.2316" x2="-15.1918" y2="-24.7684" width="0.254" layer="94"/>
<wire x1="-15.1918" y1="-24.7684" x2="15.4682" y2="-24.7684" width="0.254" layer="94"/>
<wire x1="15.4682" y1="-24.7684" x2="15.4682" y2="21.2316" width="0.254" layer="94"/>
<wire x1="15.4682" y1="21.2316" x2="-15.1918" y2="21.2316" width="0.254" layer="94"/>
<pin name="GND" x="20.32" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="GPIO23" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="GPIO22/SCL" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="GPIO1/TX" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="GPIO3/RX" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="GPIO21/SDA" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="GPIO19" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="GPIO18" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="GPIO5" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO17" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="GPIO16" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="GPIO4" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="GPIO0" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="GPIO2" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="GPIO15" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="3V3" x="-20.32" y="17.78" length="middle" direction="pwr"/>
<pin name="RESET" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="VP" x="-20.32" y="12.7" length="middle" direction="nc"/>
<pin name="VN" x="-20.32" y="10.16" length="middle" direction="nc"/>
<pin name="GPIO34" x="-20.32" y="7.62" length="middle" direction="in"/>
<pin name="GPIO32" x="-20.32" y="5.08" length="middle"/>
<pin name="GPIO33" x="-20.32" y="2.54" length="middle"/>
<pin name="GPIO25" x="-20.32" y="0" length="middle"/>
<pin name="GPIO26" x="-20.32" y="-2.54" length="middle"/>
<pin name="GPIO27" x="-20.32" y="-5.08" length="middle"/>
<pin name="GPIO14" x="-20.32" y="-7.62" length="middle"/>
<pin name="GPIO12" x="-20.32" y="-10.16" length="middle"/>
<pin name="GPIO13" x="-20.32" y="-12.7" length="middle"/>
<pin name="EN" x="-20.32" y="-15.24" length="middle" direction="in"/>
<pin name="USB" x="-20.32" y="-17.78" length="middle" direction="pwr"/>
<pin name="BAT" x="-20.32" y="-20.32" length="middle" direction="pwr"/>
<text x="-2.54" y="-27.94" size="1.27" layer="94">Lolin D32</text>
</symbol>
<symbol name="BATT_18560_HOLDER">
<description>18650 Battery Holder</description>
<pin name="P" x="0" y="5.08" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="N" x="0" y="-5.08" visible="pad" length="short" direction="pwr" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0.635" x2="4.445" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-0.635" x2="-2.54" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0" y1="-0.635" x2="2.54" y2="-0.635" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.6764" layer="95">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.6764" layer="96">&gt;VALUE</text>
<text x="-4.445" y="1.27" size="1.6764" layer="94">+</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="INA219-MODULE">
<gates>
<gate name="G$1" symbol="INA219-MODULE" x="-5.08" y="-7.62"/>
</gates>
<devices>
<device name="" package="INA219-MODULE">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VCC" pad="1"/>
<connect gate="G$1" pin="VIN+" pad="6"/>
<connect gate="G$1" pin="VIN-" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOLIND32" uservalue="yes">
<description>Wemos Lolin D32</description>
<gates>
<gate name="G$1" symbol="LOLIND32" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LOLIND32">
<connects>
<connect gate="G$1" pin="3V3" pad="1"/>
<connect gate="G$1" pin="BAT" pad="16"/>
<connect gate="G$1" pin="EN" pad="14"/>
<connect gate="G$1" pin="GND" pad="17 32" route="any"/>
<connect gate="G$1" pin="GPIO0" pad="20"/>
<connect gate="G$1" pin="GPIO1/TX" pad="29"/>
<connect gate="G$1" pin="GPIO12" pad="12"/>
<connect gate="G$1" pin="GPIO13" pad="13"/>
<connect gate="G$1" pin="GPIO14" pad="11"/>
<connect gate="G$1" pin="GPIO15" pad="18"/>
<connect gate="G$1" pin="GPIO16" pad="22"/>
<connect gate="G$1" pin="GPIO17" pad="23"/>
<connect gate="G$1" pin="GPIO18" pad="25"/>
<connect gate="G$1" pin="GPIO19" pad="26"/>
<connect gate="G$1" pin="GPIO2" pad="19"/>
<connect gate="G$1" pin="GPIO21/SDA" pad="27"/>
<connect gate="G$1" pin="GPIO22/SCL" pad="30"/>
<connect gate="G$1" pin="GPIO23" pad="31"/>
<connect gate="G$1" pin="GPIO25" pad="8"/>
<connect gate="G$1" pin="GPIO26" pad="9"/>
<connect gate="G$1" pin="GPIO27" pad="10"/>
<connect gate="G$1" pin="GPIO3/RX" pad="28"/>
<connect gate="G$1" pin="GPIO32" pad="6"/>
<connect gate="G$1" pin="GPIO33" pad="7"/>
<connect gate="G$1" pin="GPIO34" pad="5"/>
<connect gate="G$1" pin="GPIO4" pad="21"/>
<connect gate="G$1" pin="GPIO5" pad="24"/>
<connect gate="G$1" pin="RESET" pad="2"/>
<connect gate="G$1" pin="USB" pad="15"/>
<connect gate="G$1" pin="VN" pad="4"/>
<connect gate="G$1" pin="VP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATT_18650_HOLDER" prefix="C" uservalue="yes">
<gates>
<gate name="P" symbol="BATT_18560_HOLDER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATT_18560_HOLDER_DIP">
<connects>
<connect gate="P" pin="N" pad="N"/>
<connect gate="P" pin="P" pad="P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="BANANA_CONN">
<description>&lt;h3&gt;Banana Plug PTH&lt;/h3&gt;
0.2" diameter pad, 0.1" hole. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 1&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;BANANA_CONN&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="7.125" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="3.81" diameter="8"/>
<text x="-3.81" y="4.572" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-3.81" y="-5.207" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<circle x="0" y="0" radius="7.125" width="0.127" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="BANANA_CONN">
<description>&lt;h3&gt;Through-hole Banana Jack&lt;/h3&gt;
&lt;p&gt;Basic PTH connection for banana jack plugs&lt;/p&gt;</description>
<circle x="0" y="0" radius="1.2951" width="0.254" layer="94"/>
<text x="-1.016" y="1.778" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-1.016" y="-3.048" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BANANA_CONN" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Through-hole Banana Jack&lt;/h3&gt;
&lt;p&gt;Basic PTH connection for banana jack plugs&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/retired/10956"&gt;Multimeter Kit&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/retired/509"&gt;Banana to Alligator Cable&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/retired/508"&gt;Banana to Alligator Coax Cable&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/retired/507"&gt;Banana to Banana Cable&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/retired/506"&gt;Banana to IC Hook Cables&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="BANANA_CONN" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="BANANA_CONN">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#afb-Aesthetics">
<packages>
<package name="LOGO_ITS">
<rectangle x1="9.325" y1="-0.105" x2="9.655" y2="-0.095" layer="21"/>
<rectangle x1="9.085" y1="-0.095" x2="9.875" y2="-0.085" layer="21"/>
<rectangle x1="8.945" y1="-0.085" x2="10.005" y2="-0.075" layer="21"/>
<rectangle x1="8.835" y1="-0.075" x2="10.105" y2="-0.065" layer="21"/>
<rectangle x1="8.755" y1="-0.065" x2="10.185" y2="-0.055" layer="21"/>
<rectangle x1="8.675" y1="-0.055" x2="10.265" y2="-0.045" layer="21"/>
<rectangle x1="8.595" y1="-0.045" x2="10.325" y2="-0.035" layer="21"/>
<rectangle x1="8.525" y1="-0.035" x2="10.385" y2="-0.025" layer="21"/>
<rectangle x1="8.455" y1="-0.025" x2="10.445" y2="-0.015" layer="21"/>
<rectangle x1="0.005" y1="-0.015" x2="1.845" y2="-0.005" layer="21"/>
<rectangle x1="3.805" y1="-0.015" x2="5.605" y2="-0.005" layer="21"/>
<rectangle x1="8.395" y1="-0.015" x2="10.495" y2="-0.005" layer="21"/>
<rectangle x1="0.015" y1="-0.005" x2="1.835" y2="0.005" layer="21"/>
<rectangle x1="3.815" y1="-0.005" x2="5.605" y2="0.005" layer="21"/>
<rectangle x1="8.325" y1="-0.005" x2="10.545" y2="0.005" layer="21"/>
<rectangle x1="0.025" y1="0.005" x2="1.825" y2="0.015" layer="21"/>
<rectangle x1="3.825" y1="0.005" x2="5.595" y2="0.015" layer="21"/>
<rectangle x1="8.275" y1="0.005" x2="10.585" y2="0.015" layer="21"/>
<rectangle x1="0.045" y1="0.015" x2="1.825" y2="0.025" layer="21"/>
<rectangle x1="3.835" y1="0.015" x2="5.585" y2="0.025" layer="21"/>
<rectangle x1="8.215" y1="0.015" x2="10.625" y2="0.025" layer="21"/>
<rectangle x1="0.055" y1="0.025" x2="1.815" y2="0.035" layer="21"/>
<rectangle x1="3.835" y1="0.025" x2="5.575" y2="0.035" layer="21"/>
<rectangle x1="8.155" y1="0.025" x2="10.665" y2="0.035" layer="21"/>
<rectangle x1="0.065" y1="0.035" x2="1.805" y2="0.045" layer="21"/>
<rectangle x1="3.845" y1="0.035" x2="5.565" y2="0.045" layer="21"/>
<rectangle x1="8.095" y1="0.035" x2="10.705" y2="0.045" layer="21"/>
<rectangle x1="0.075" y1="0.045" x2="1.795" y2="0.055" layer="21"/>
<rectangle x1="3.855" y1="0.045" x2="5.565" y2="0.055" layer="21"/>
<rectangle x1="8.045" y1="0.045" x2="10.745" y2="0.055" layer="21"/>
<rectangle x1="0.085" y1="0.055" x2="1.785" y2="0.065" layer="21"/>
<rectangle x1="3.865" y1="0.055" x2="5.555" y2="0.065" layer="21"/>
<rectangle x1="7.995" y1="0.055" x2="10.785" y2="0.065" layer="21"/>
<rectangle x1="0.095" y1="0.065" x2="1.785" y2="0.075" layer="21"/>
<rectangle x1="3.875" y1="0.065" x2="5.545" y2="0.075" layer="21"/>
<rectangle x1="7.945" y1="0.065" x2="10.815" y2="0.075" layer="21"/>
<rectangle x1="0.105" y1="0.075" x2="1.775" y2="0.085" layer="21"/>
<rectangle x1="3.885" y1="0.075" x2="5.535" y2="0.085" layer="21"/>
<rectangle x1="7.895" y1="0.075" x2="10.845" y2="0.085" layer="21"/>
<rectangle x1="0.115" y1="0.085" x2="1.765" y2="0.095" layer="21"/>
<rectangle x1="3.895" y1="0.085" x2="5.525" y2="0.095" layer="21"/>
<rectangle x1="7.845" y1="0.085" x2="10.875" y2="0.095" layer="21"/>
<rectangle x1="0.125" y1="0.095" x2="1.755" y2="0.105" layer="21"/>
<rectangle x1="3.905" y1="0.095" x2="5.525" y2="0.105" layer="21"/>
<rectangle x1="7.805" y1="0.095" x2="10.915" y2="0.105" layer="21"/>
<rectangle x1="0.135" y1="0.105" x2="1.745" y2="0.115" layer="21"/>
<rectangle x1="3.905" y1="0.105" x2="5.515" y2="0.115" layer="21"/>
<rectangle x1="7.755" y1="0.105" x2="10.945" y2="0.115" layer="21"/>
<rectangle x1="0.145" y1="0.115" x2="1.745" y2="0.125" layer="21"/>
<rectangle x1="3.915" y1="0.115" x2="5.505" y2="0.125" layer="21"/>
<rectangle x1="7.725" y1="0.115" x2="10.965" y2="0.125" layer="21"/>
<rectangle x1="0.155" y1="0.125" x2="1.735" y2="0.135" layer="21"/>
<rectangle x1="3.925" y1="0.125" x2="5.495" y2="0.135" layer="21"/>
<rectangle x1="7.685" y1="0.125" x2="10.995" y2="0.135" layer="21"/>
<rectangle x1="0.155" y1="0.135" x2="1.725" y2="0.145" layer="21"/>
<rectangle x1="3.935" y1="0.135" x2="5.495" y2="0.145" layer="21"/>
<rectangle x1="7.645" y1="0.135" x2="11.025" y2="0.145" layer="21"/>
<rectangle x1="0.165" y1="0.145" x2="1.715" y2="0.155" layer="21"/>
<rectangle x1="3.945" y1="0.145" x2="5.485" y2="0.155" layer="21"/>
<rectangle x1="7.615" y1="0.145" x2="11.045" y2="0.155" layer="21"/>
<rectangle x1="0.175" y1="0.155" x2="1.705" y2="0.165" layer="21"/>
<rectangle x1="3.955" y1="0.155" x2="5.485" y2="0.165" layer="21"/>
<rectangle x1="7.595" y1="0.155" x2="11.075" y2="0.165" layer="21"/>
<rectangle x1="0.175" y1="0.165" x2="1.705" y2="0.175" layer="21"/>
<rectangle x1="3.965" y1="0.165" x2="5.475" y2="0.175" layer="21"/>
<rectangle x1="7.575" y1="0.165" x2="11.105" y2="0.175" layer="21"/>
<rectangle x1="0.185" y1="0.175" x2="1.695" y2="0.185" layer="21"/>
<rectangle x1="3.975" y1="0.175" x2="5.465" y2="0.185" layer="21"/>
<rectangle x1="7.565" y1="0.175" x2="11.125" y2="0.185" layer="21"/>
<rectangle x1="0.195" y1="0.185" x2="1.685" y2="0.195" layer="21"/>
<rectangle x1="3.975" y1="0.185" x2="5.465" y2="0.195" layer="21"/>
<rectangle x1="7.565" y1="0.185" x2="11.145" y2="0.195" layer="21"/>
<rectangle x1="0.195" y1="0.195" x2="1.675" y2="0.205" layer="21"/>
<rectangle x1="3.985" y1="0.195" x2="5.465" y2="0.205" layer="21"/>
<rectangle x1="7.555" y1="0.195" x2="11.175" y2="0.205" layer="21"/>
<rectangle x1="0.205" y1="0.205" x2="1.665" y2="0.215" layer="21"/>
<rectangle x1="3.985" y1="0.205" x2="5.455" y2="0.215" layer="21"/>
<rectangle x1="7.555" y1="0.205" x2="11.195" y2="0.215" layer="21"/>
<rectangle x1="0.205" y1="0.215" x2="1.665" y2="0.225" layer="21"/>
<rectangle x1="3.985" y1="0.215" x2="5.455" y2="0.225" layer="21"/>
<rectangle x1="7.555" y1="0.215" x2="11.215" y2="0.225" layer="21"/>
<rectangle x1="0.205" y1="0.225" x2="1.655" y2="0.235" layer="21"/>
<rectangle x1="3.985" y1="0.225" x2="5.445" y2="0.235" layer="21"/>
<rectangle x1="7.545" y1="0.225" x2="11.235" y2="0.235" layer="21"/>
<rectangle x1="0.205" y1="0.235" x2="1.655" y2="0.245" layer="21"/>
<rectangle x1="3.985" y1="0.235" x2="5.445" y2="0.245" layer="21"/>
<rectangle x1="7.545" y1="0.235" x2="11.255" y2="0.245" layer="21"/>
<rectangle x1="0.205" y1="0.245" x2="1.655" y2="0.255" layer="21"/>
<rectangle x1="3.985" y1="0.245" x2="5.445" y2="0.255" layer="21"/>
<rectangle x1="7.535" y1="0.245" x2="11.275" y2="0.255" layer="21"/>
<rectangle x1="0.205" y1="0.255" x2="1.655" y2="0.265" layer="21"/>
<rectangle x1="3.985" y1="0.255" x2="5.435" y2="0.265" layer="21"/>
<rectangle x1="7.535" y1="0.255" x2="11.295" y2="0.265" layer="21"/>
<rectangle x1="0.205" y1="0.265" x2="1.655" y2="0.275" layer="21"/>
<rectangle x1="3.985" y1="0.265" x2="5.435" y2="0.275" layer="21"/>
<rectangle x1="7.535" y1="0.265" x2="11.315" y2="0.275" layer="21"/>
<rectangle x1="0.215" y1="0.275" x2="1.655" y2="0.285" layer="21"/>
<rectangle x1="3.985" y1="0.275" x2="5.435" y2="0.285" layer="21"/>
<rectangle x1="7.525" y1="0.275" x2="11.335" y2="0.285" layer="21"/>
<rectangle x1="0.215" y1="0.285" x2="1.655" y2="0.295" layer="21"/>
<rectangle x1="3.985" y1="0.285" x2="5.435" y2="0.295" layer="21"/>
<rectangle x1="7.525" y1="0.285" x2="11.355" y2="0.295" layer="21"/>
<rectangle x1="0.215" y1="0.295" x2="1.655" y2="0.305" layer="21"/>
<rectangle x1="3.985" y1="0.295" x2="5.425" y2="0.305" layer="21"/>
<rectangle x1="7.515" y1="0.295" x2="11.375" y2="0.305" layer="21"/>
<rectangle x1="0.215" y1="0.305" x2="1.655" y2="0.315" layer="21"/>
<rectangle x1="3.985" y1="0.305" x2="5.425" y2="0.315" layer="21"/>
<rectangle x1="7.515" y1="0.305" x2="11.385" y2="0.315" layer="21"/>
<rectangle x1="0.215" y1="0.315" x2="1.655" y2="0.325" layer="21"/>
<rectangle x1="3.985" y1="0.315" x2="5.425" y2="0.325" layer="21"/>
<rectangle x1="7.515" y1="0.315" x2="11.405" y2="0.325" layer="21"/>
<rectangle x1="0.215" y1="0.325" x2="1.655" y2="0.335" layer="21"/>
<rectangle x1="3.985" y1="0.325" x2="5.425" y2="0.335" layer="21"/>
<rectangle x1="7.505" y1="0.325" x2="11.425" y2="0.335" layer="21"/>
<rectangle x1="0.215" y1="0.335" x2="1.655" y2="0.345" layer="21"/>
<rectangle x1="3.985" y1="0.335" x2="5.415" y2="0.345" layer="21"/>
<rectangle x1="7.505" y1="0.335" x2="11.445" y2="0.345" layer="21"/>
<rectangle x1="0.215" y1="0.345" x2="1.655" y2="0.355" layer="21"/>
<rectangle x1="3.985" y1="0.345" x2="5.415" y2="0.355" layer="21"/>
<rectangle x1="7.505" y1="0.345" x2="11.455" y2="0.355" layer="21"/>
<rectangle x1="0.215" y1="0.355" x2="1.655" y2="0.365" layer="21"/>
<rectangle x1="3.985" y1="0.355" x2="5.415" y2="0.365" layer="21"/>
<rectangle x1="7.495" y1="0.355" x2="11.475" y2="0.365" layer="21"/>
<rectangle x1="0.215" y1="0.365" x2="1.655" y2="0.375" layer="21"/>
<rectangle x1="3.985" y1="0.365" x2="5.415" y2="0.375" layer="21"/>
<rectangle x1="7.495" y1="0.365" x2="11.485" y2="0.375" layer="21"/>
<rectangle x1="0.215" y1="0.375" x2="1.655" y2="0.385" layer="21"/>
<rectangle x1="3.985" y1="0.375" x2="5.415" y2="0.385" layer="21"/>
<rectangle x1="7.495" y1="0.375" x2="11.505" y2="0.385" layer="21"/>
<rectangle x1="0.215" y1="0.385" x2="1.655" y2="0.395" layer="21"/>
<rectangle x1="3.985" y1="0.385" x2="5.415" y2="0.395" layer="21"/>
<rectangle x1="7.485" y1="0.385" x2="11.515" y2="0.395" layer="21"/>
<rectangle x1="0.215" y1="0.395" x2="1.655" y2="0.405" layer="21"/>
<rectangle x1="3.985" y1="0.395" x2="5.415" y2="0.405" layer="21"/>
<rectangle x1="7.485" y1="0.395" x2="11.535" y2="0.405" layer="21"/>
<rectangle x1="0.215" y1="0.405" x2="1.655" y2="0.415" layer="21"/>
<rectangle x1="3.985" y1="0.405" x2="5.405" y2="0.415" layer="21"/>
<rectangle x1="7.485" y1="0.405" x2="11.545" y2="0.415" layer="21"/>
<rectangle x1="0.215" y1="0.415" x2="1.655" y2="0.425" layer="21"/>
<rectangle x1="3.985" y1="0.415" x2="5.405" y2="0.425" layer="21"/>
<rectangle x1="7.475" y1="0.415" x2="11.565" y2="0.425" layer="21"/>
<rectangle x1="0.215" y1="0.425" x2="1.655" y2="0.435" layer="21"/>
<rectangle x1="3.985" y1="0.425" x2="5.405" y2="0.435" layer="21"/>
<rectangle x1="7.475" y1="0.425" x2="11.575" y2="0.435" layer="21"/>
<rectangle x1="0.215" y1="0.435" x2="1.655" y2="0.445" layer="21"/>
<rectangle x1="3.985" y1="0.435" x2="5.405" y2="0.445" layer="21"/>
<rectangle x1="7.475" y1="0.435" x2="11.595" y2="0.445" layer="21"/>
<rectangle x1="0.215" y1="0.445" x2="1.655" y2="0.455" layer="21"/>
<rectangle x1="3.985" y1="0.445" x2="5.405" y2="0.455" layer="21"/>
<rectangle x1="7.465" y1="0.445" x2="11.605" y2="0.455" layer="21"/>
<rectangle x1="0.215" y1="0.455" x2="1.655" y2="0.465" layer="21"/>
<rectangle x1="3.985" y1="0.455" x2="5.405" y2="0.465" layer="21"/>
<rectangle x1="7.465" y1="0.455" x2="11.615" y2="0.465" layer="21"/>
<rectangle x1="0.215" y1="0.465" x2="1.655" y2="0.475" layer="21"/>
<rectangle x1="3.985" y1="0.465" x2="5.405" y2="0.475" layer="21"/>
<rectangle x1="7.465" y1="0.465" x2="11.635" y2="0.475" layer="21"/>
<rectangle x1="0.225" y1="0.475" x2="1.655" y2="0.485" layer="21"/>
<rectangle x1="3.985" y1="0.475" x2="5.405" y2="0.485" layer="21"/>
<rectangle x1="7.455" y1="0.475" x2="11.645" y2="0.485" layer="21"/>
<rectangle x1="0.225" y1="0.485" x2="1.655" y2="0.495" layer="21"/>
<rectangle x1="3.985" y1="0.485" x2="5.405" y2="0.495" layer="21"/>
<rectangle x1="7.455" y1="0.485" x2="11.655" y2="0.495" layer="21"/>
<rectangle x1="0.225" y1="0.495" x2="1.655" y2="0.505" layer="21"/>
<rectangle x1="3.985" y1="0.495" x2="5.395" y2="0.505" layer="21"/>
<rectangle x1="7.455" y1="0.495" x2="11.675" y2="0.505" layer="21"/>
<rectangle x1="0.225" y1="0.505" x2="1.655" y2="0.515" layer="21"/>
<rectangle x1="3.985" y1="0.505" x2="5.395" y2="0.515" layer="21"/>
<rectangle x1="7.445" y1="0.505" x2="11.685" y2="0.515" layer="21"/>
<rectangle x1="0.225" y1="0.515" x2="1.655" y2="0.525" layer="21"/>
<rectangle x1="3.985" y1="0.515" x2="5.395" y2="0.525" layer="21"/>
<rectangle x1="7.445" y1="0.515" x2="11.695" y2="0.525" layer="21"/>
<rectangle x1="0.225" y1="0.525" x2="1.655" y2="0.535" layer="21"/>
<rectangle x1="3.985" y1="0.525" x2="5.395" y2="0.535" layer="21"/>
<rectangle x1="7.445" y1="0.525" x2="11.705" y2="0.535" layer="21"/>
<rectangle x1="0.225" y1="0.535" x2="1.655" y2="0.545" layer="21"/>
<rectangle x1="3.985" y1="0.535" x2="5.395" y2="0.545" layer="21"/>
<rectangle x1="7.445" y1="0.535" x2="11.715" y2="0.545" layer="21"/>
<rectangle x1="0.225" y1="0.545" x2="1.655" y2="0.555" layer="21"/>
<rectangle x1="3.985" y1="0.545" x2="5.395" y2="0.555" layer="21"/>
<rectangle x1="7.435" y1="0.545" x2="11.725" y2="0.555" layer="21"/>
<rectangle x1="0.225" y1="0.555" x2="1.655" y2="0.565" layer="21"/>
<rectangle x1="3.985" y1="0.555" x2="5.395" y2="0.565" layer="21"/>
<rectangle x1="7.435" y1="0.555" x2="11.745" y2="0.565" layer="21"/>
<rectangle x1="0.225" y1="0.565" x2="1.655" y2="0.575" layer="21"/>
<rectangle x1="3.985" y1="0.565" x2="5.395" y2="0.575" layer="21"/>
<rectangle x1="7.435" y1="0.565" x2="11.755" y2="0.575" layer="21"/>
<rectangle x1="0.225" y1="0.575" x2="1.655" y2="0.585" layer="21"/>
<rectangle x1="3.985" y1="0.575" x2="5.395" y2="0.585" layer="21"/>
<rectangle x1="7.425" y1="0.575" x2="11.765" y2="0.585" layer="21"/>
<rectangle x1="0.225" y1="0.585" x2="1.655" y2="0.595" layer="21"/>
<rectangle x1="3.985" y1="0.585" x2="5.395" y2="0.595" layer="21"/>
<rectangle x1="7.425" y1="0.585" x2="11.775" y2="0.595" layer="21"/>
<rectangle x1="0.225" y1="0.595" x2="1.655" y2="0.605" layer="21"/>
<rectangle x1="3.985" y1="0.595" x2="5.395" y2="0.605" layer="21"/>
<rectangle x1="7.425" y1="0.595" x2="11.785" y2="0.605" layer="21"/>
<rectangle x1="0.225" y1="0.605" x2="1.655" y2="0.615" layer="21"/>
<rectangle x1="3.985" y1="0.605" x2="5.395" y2="0.615" layer="21"/>
<rectangle x1="7.415" y1="0.605" x2="11.795" y2="0.615" layer="21"/>
<rectangle x1="0.225" y1="0.615" x2="1.655" y2="0.625" layer="21"/>
<rectangle x1="3.985" y1="0.615" x2="5.395" y2="0.625" layer="21"/>
<rectangle x1="7.415" y1="0.615" x2="11.805" y2="0.625" layer="21"/>
<rectangle x1="0.225" y1="0.625" x2="1.655" y2="0.635" layer="21"/>
<rectangle x1="3.985" y1="0.625" x2="5.395" y2="0.635" layer="21"/>
<rectangle x1="7.415" y1="0.625" x2="11.815" y2="0.635" layer="21"/>
<rectangle x1="0.225" y1="0.635" x2="1.655" y2="0.645" layer="21"/>
<rectangle x1="3.985" y1="0.635" x2="5.385" y2="0.645" layer="21"/>
<rectangle x1="7.415" y1="0.635" x2="11.825" y2="0.645" layer="21"/>
<rectangle x1="0.225" y1="0.645" x2="1.655" y2="0.655" layer="21"/>
<rectangle x1="3.985" y1="0.645" x2="5.385" y2="0.655" layer="21"/>
<rectangle x1="7.405" y1="0.645" x2="11.835" y2="0.655" layer="21"/>
<rectangle x1="0.225" y1="0.655" x2="1.655" y2="0.665" layer="21"/>
<rectangle x1="3.985" y1="0.655" x2="5.385" y2="0.665" layer="21"/>
<rectangle x1="7.405" y1="0.655" x2="11.845" y2="0.665" layer="21"/>
<rectangle x1="0.225" y1="0.665" x2="1.655" y2="0.675" layer="21"/>
<rectangle x1="3.985" y1="0.665" x2="5.385" y2="0.675" layer="21"/>
<rectangle x1="7.405" y1="0.665" x2="11.855" y2="0.675" layer="21"/>
<rectangle x1="0.225" y1="0.675" x2="1.655" y2="0.685" layer="21"/>
<rectangle x1="3.985" y1="0.675" x2="5.385" y2="0.685" layer="21"/>
<rectangle x1="7.395" y1="0.675" x2="11.865" y2="0.685" layer="21"/>
<rectangle x1="0.225" y1="0.685" x2="1.655" y2="0.695" layer="21"/>
<rectangle x1="3.985" y1="0.685" x2="5.385" y2="0.695" layer="21"/>
<rectangle x1="7.395" y1="0.685" x2="11.875" y2="0.695" layer="21"/>
<rectangle x1="0.225" y1="0.695" x2="1.655" y2="0.705" layer="21"/>
<rectangle x1="3.985" y1="0.695" x2="5.385" y2="0.705" layer="21"/>
<rectangle x1="7.395" y1="0.695" x2="11.885" y2="0.705" layer="21"/>
<rectangle x1="0.225" y1="0.705" x2="1.655" y2="0.715" layer="21"/>
<rectangle x1="3.985" y1="0.705" x2="5.385" y2="0.715" layer="21"/>
<rectangle x1="7.385" y1="0.705" x2="11.895" y2="0.715" layer="21"/>
<rectangle x1="0.225" y1="0.715" x2="1.655" y2="0.725" layer="21"/>
<rectangle x1="3.985" y1="0.715" x2="5.385" y2="0.725" layer="21"/>
<rectangle x1="7.385" y1="0.715" x2="11.895" y2="0.725" layer="21"/>
<rectangle x1="0.225" y1="0.725" x2="1.655" y2="0.735" layer="21"/>
<rectangle x1="3.985" y1="0.725" x2="5.385" y2="0.735" layer="21"/>
<rectangle x1="7.385" y1="0.725" x2="11.905" y2="0.735" layer="21"/>
<rectangle x1="0.225" y1="0.735" x2="1.655" y2="0.745" layer="21"/>
<rectangle x1="3.985" y1="0.735" x2="5.385" y2="0.745" layer="21"/>
<rectangle x1="7.385" y1="0.735" x2="11.915" y2="0.745" layer="21"/>
<rectangle x1="0.225" y1="0.745" x2="1.655" y2="0.755" layer="21"/>
<rectangle x1="3.985" y1="0.745" x2="5.385" y2="0.755" layer="21"/>
<rectangle x1="7.375" y1="0.745" x2="11.925" y2="0.755" layer="21"/>
<rectangle x1="0.225" y1="0.755" x2="1.655" y2="0.765" layer="21"/>
<rectangle x1="3.985" y1="0.755" x2="5.385" y2="0.765" layer="21"/>
<rectangle x1="7.375" y1="0.755" x2="11.935" y2="0.765" layer="21"/>
<rectangle x1="0.225" y1="0.765" x2="1.655" y2="0.775" layer="21"/>
<rectangle x1="3.985" y1="0.765" x2="5.385" y2="0.775" layer="21"/>
<rectangle x1="7.375" y1="0.765" x2="11.945" y2="0.775" layer="21"/>
<rectangle x1="0.225" y1="0.775" x2="1.655" y2="0.785" layer="21"/>
<rectangle x1="3.985" y1="0.775" x2="5.385" y2="0.785" layer="21"/>
<rectangle x1="7.365" y1="0.775" x2="11.945" y2="0.785" layer="21"/>
<rectangle x1="0.225" y1="0.785" x2="1.655" y2="0.795" layer="21"/>
<rectangle x1="3.985" y1="0.785" x2="5.385" y2="0.795" layer="21"/>
<rectangle x1="7.365" y1="0.785" x2="11.955" y2="0.795" layer="21"/>
<rectangle x1="0.225" y1="0.795" x2="1.655" y2="0.805" layer="21"/>
<rectangle x1="3.985" y1="0.795" x2="5.385" y2="0.805" layer="21"/>
<rectangle x1="7.365" y1="0.795" x2="11.965" y2="0.805" layer="21"/>
<rectangle x1="0.225" y1="0.805" x2="1.655" y2="0.815" layer="21"/>
<rectangle x1="3.985" y1="0.805" x2="5.385" y2="0.815" layer="21"/>
<rectangle x1="7.365" y1="0.805" x2="11.975" y2="0.815" layer="21"/>
<rectangle x1="0.225" y1="0.815" x2="1.655" y2="0.825" layer="21"/>
<rectangle x1="3.985" y1="0.815" x2="5.385" y2="0.825" layer="21"/>
<rectangle x1="7.355" y1="0.815" x2="11.975" y2="0.825" layer="21"/>
<rectangle x1="0.225" y1="0.825" x2="1.655" y2="0.835" layer="21"/>
<rectangle x1="3.985" y1="0.825" x2="5.385" y2="0.835" layer="21"/>
<rectangle x1="7.355" y1="0.825" x2="11.985" y2="0.835" layer="21"/>
<rectangle x1="0.225" y1="0.835" x2="1.655" y2="0.845" layer="21"/>
<rectangle x1="3.985" y1="0.835" x2="5.385" y2="0.845" layer="21"/>
<rectangle x1="7.355" y1="0.835" x2="11.995" y2="0.845" layer="21"/>
<rectangle x1="0.225" y1="0.845" x2="1.655" y2="0.855" layer="21"/>
<rectangle x1="3.985" y1="0.845" x2="5.385" y2="0.855" layer="21"/>
<rectangle x1="7.345" y1="0.845" x2="12.005" y2="0.855" layer="21"/>
<rectangle x1="0.225" y1="0.855" x2="1.655" y2="0.865" layer="21"/>
<rectangle x1="3.985" y1="0.855" x2="5.385" y2="0.865" layer="21"/>
<rectangle x1="7.345" y1="0.855" x2="12.005" y2="0.865" layer="21"/>
<rectangle x1="0.225" y1="0.865" x2="1.655" y2="0.875" layer="21"/>
<rectangle x1="3.985" y1="0.865" x2="5.385" y2="0.875" layer="21"/>
<rectangle x1="7.345" y1="0.865" x2="9.495" y2="0.875" layer="21"/>
<rectangle x1="9.805" y1="0.865" x2="12.015" y2="0.875" layer="21"/>
<rectangle x1="0.235" y1="0.875" x2="1.655" y2="0.885" layer="21"/>
<rectangle x1="3.985" y1="0.875" x2="5.385" y2="0.885" layer="21"/>
<rectangle x1="7.345" y1="0.875" x2="9.265" y2="0.885" layer="21"/>
<rectangle x1="9.985" y1="0.875" x2="12.025" y2="0.885" layer="21"/>
<rectangle x1="0.235" y1="0.885" x2="1.655" y2="0.895" layer="21"/>
<rectangle x1="3.985" y1="0.885" x2="5.385" y2="0.895" layer="21"/>
<rectangle x1="7.335" y1="0.885" x2="9.115" y2="0.895" layer="21"/>
<rectangle x1="10.075" y1="0.885" x2="12.025" y2="0.895" layer="21"/>
<rectangle x1="0.235" y1="0.895" x2="1.655" y2="0.905" layer="21"/>
<rectangle x1="3.985" y1="0.895" x2="5.385" y2="0.905" layer="21"/>
<rectangle x1="7.335" y1="0.895" x2="8.995" y2="0.905" layer="21"/>
<rectangle x1="10.145" y1="0.895" x2="12.035" y2="0.905" layer="21"/>
<rectangle x1="0.235" y1="0.905" x2="1.655" y2="0.915" layer="21"/>
<rectangle x1="3.985" y1="0.905" x2="5.385" y2="0.915" layer="21"/>
<rectangle x1="7.335" y1="0.905" x2="8.885" y2="0.915" layer="21"/>
<rectangle x1="10.205" y1="0.905" x2="12.045" y2="0.915" layer="21"/>
<rectangle x1="0.235" y1="0.915" x2="1.655" y2="0.925" layer="21"/>
<rectangle x1="3.985" y1="0.915" x2="5.385" y2="0.925" layer="21"/>
<rectangle x1="7.325" y1="0.915" x2="8.785" y2="0.925" layer="21"/>
<rectangle x1="10.245" y1="0.915" x2="12.045" y2="0.925" layer="21"/>
<rectangle x1="0.235" y1="0.925" x2="1.655" y2="0.935" layer="21"/>
<rectangle x1="3.985" y1="0.925" x2="5.385" y2="0.935" layer="21"/>
<rectangle x1="7.325" y1="0.925" x2="8.695" y2="0.935" layer="21"/>
<rectangle x1="10.295" y1="0.925" x2="12.055" y2="0.935" layer="21"/>
<rectangle x1="0.235" y1="0.935" x2="1.655" y2="0.945" layer="21"/>
<rectangle x1="3.985" y1="0.935" x2="5.385" y2="0.945" layer="21"/>
<rectangle x1="7.325" y1="0.935" x2="8.615" y2="0.945" layer="21"/>
<rectangle x1="10.335" y1="0.935" x2="12.065" y2="0.945" layer="21"/>
<rectangle x1="0.235" y1="0.945" x2="1.655" y2="0.955" layer="21"/>
<rectangle x1="3.985" y1="0.945" x2="5.385" y2="0.955" layer="21"/>
<rectangle x1="7.315" y1="0.945" x2="8.535" y2="0.955" layer="21"/>
<rectangle x1="10.365" y1="0.945" x2="12.065" y2="0.955" layer="21"/>
<rectangle x1="0.235" y1="0.955" x2="1.655" y2="0.965" layer="21"/>
<rectangle x1="3.985" y1="0.955" x2="5.385" y2="0.965" layer="21"/>
<rectangle x1="7.315" y1="0.955" x2="8.465" y2="0.965" layer="21"/>
<rectangle x1="10.395" y1="0.955" x2="12.075" y2="0.965" layer="21"/>
<rectangle x1="0.235" y1="0.965" x2="1.655" y2="0.975" layer="21"/>
<rectangle x1="3.985" y1="0.965" x2="5.385" y2="0.975" layer="21"/>
<rectangle x1="7.315" y1="0.965" x2="8.395" y2="0.975" layer="21"/>
<rectangle x1="10.425" y1="0.965" x2="12.075" y2="0.975" layer="21"/>
<rectangle x1="0.235" y1="0.975" x2="1.655" y2="0.985" layer="21"/>
<rectangle x1="3.985" y1="0.975" x2="5.385" y2="0.985" layer="21"/>
<rectangle x1="7.315" y1="0.975" x2="8.325" y2="0.985" layer="21"/>
<rectangle x1="10.455" y1="0.975" x2="12.085" y2="0.985" layer="21"/>
<rectangle x1="0.235" y1="0.985" x2="1.655" y2="0.995" layer="21"/>
<rectangle x1="3.985" y1="0.985" x2="5.385" y2="0.995" layer="21"/>
<rectangle x1="7.305" y1="0.985" x2="8.265" y2="0.995" layer="21"/>
<rectangle x1="10.485" y1="0.985" x2="12.095" y2="0.995" layer="21"/>
<rectangle x1="0.235" y1="0.995" x2="1.655" y2="1.005" layer="21"/>
<rectangle x1="3.985" y1="0.995" x2="5.385" y2="1.005" layer="21"/>
<rectangle x1="7.305" y1="0.995" x2="8.205" y2="1.005" layer="21"/>
<rectangle x1="10.505" y1="0.995" x2="12.095" y2="1.005" layer="21"/>
<rectangle x1="0.235" y1="1.005" x2="1.655" y2="1.015" layer="21"/>
<rectangle x1="3.985" y1="1.005" x2="5.385" y2="1.015" layer="21"/>
<rectangle x1="7.305" y1="1.005" x2="8.145" y2="1.015" layer="21"/>
<rectangle x1="10.525" y1="1.005" x2="12.105" y2="1.015" layer="21"/>
<rectangle x1="0.235" y1="1.015" x2="1.655" y2="1.025" layer="21"/>
<rectangle x1="3.985" y1="1.015" x2="5.385" y2="1.025" layer="21"/>
<rectangle x1="7.295" y1="1.015" x2="8.085" y2="1.025" layer="21"/>
<rectangle x1="10.555" y1="1.015" x2="12.105" y2="1.025" layer="21"/>
<rectangle x1="0.235" y1="1.025" x2="1.655" y2="1.035" layer="21"/>
<rectangle x1="3.985" y1="1.025" x2="5.385" y2="1.035" layer="21"/>
<rectangle x1="7.295" y1="1.025" x2="8.025" y2="1.035" layer="21"/>
<rectangle x1="10.575" y1="1.025" x2="12.115" y2="1.035" layer="21"/>
<rectangle x1="0.235" y1="1.035" x2="1.655" y2="1.045" layer="21"/>
<rectangle x1="3.985" y1="1.035" x2="5.385" y2="1.045" layer="21"/>
<rectangle x1="7.295" y1="1.035" x2="7.975" y2="1.045" layer="21"/>
<rectangle x1="10.595" y1="1.035" x2="12.115" y2="1.045" layer="21"/>
<rectangle x1="0.235" y1="1.045" x2="1.655" y2="1.055" layer="21"/>
<rectangle x1="3.985" y1="1.045" x2="5.375" y2="1.055" layer="21"/>
<rectangle x1="7.285" y1="1.045" x2="7.925" y2="1.055" layer="21"/>
<rectangle x1="10.605" y1="1.045" x2="12.125" y2="1.055" layer="21"/>
<rectangle x1="0.235" y1="1.055" x2="1.655" y2="1.065" layer="21"/>
<rectangle x1="3.985" y1="1.055" x2="5.375" y2="1.065" layer="21"/>
<rectangle x1="7.285" y1="1.055" x2="7.875" y2="1.065" layer="21"/>
<rectangle x1="10.625" y1="1.055" x2="12.135" y2="1.065" layer="21"/>
<rectangle x1="0.235" y1="1.065" x2="1.655" y2="1.075" layer="21"/>
<rectangle x1="3.985" y1="1.065" x2="5.375" y2="1.075" layer="21"/>
<rectangle x1="7.285" y1="1.065" x2="7.825" y2="1.075" layer="21"/>
<rectangle x1="10.645" y1="1.065" x2="12.135" y2="1.075" layer="21"/>
<rectangle x1="0.235" y1="1.075" x2="1.655" y2="1.085" layer="21"/>
<rectangle x1="3.985" y1="1.075" x2="5.375" y2="1.085" layer="21"/>
<rectangle x1="7.285" y1="1.075" x2="7.775" y2="1.085" layer="21"/>
<rectangle x1="10.655" y1="1.075" x2="12.145" y2="1.085" layer="21"/>
<rectangle x1="0.235" y1="1.085" x2="1.655" y2="1.095" layer="21"/>
<rectangle x1="3.985" y1="1.085" x2="5.375" y2="1.095" layer="21"/>
<rectangle x1="7.275" y1="1.085" x2="7.735" y2="1.095" layer="21"/>
<rectangle x1="10.675" y1="1.085" x2="12.145" y2="1.095" layer="21"/>
<rectangle x1="0.235" y1="1.095" x2="1.655" y2="1.105" layer="21"/>
<rectangle x1="3.985" y1="1.095" x2="5.375" y2="1.105" layer="21"/>
<rectangle x1="7.275" y1="1.095" x2="7.685" y2="1.105" layer="21"/>
<rectangle x1="10.685" y1="1.095" x2="12.155" y2="1.105" layer="21"/>
<rectangle x1="0.235" y1="1.105" x2="1.655" y2="1.115" layer="21"/>
<rectangle x1="3.985" y1="1.105" x2="5.375" y2="1.115" layer="21"/>
<rectangle x1="7.275" y1="1.105" x2="7.645" y2="1.115" layer="21"/>
<rectangle x1="10.705" y1="1.105" x2="12.155" y2="1.115" layer="21"/>
<rectangle x1="0.235" y1="1.115" x2="1.655" y2="1.125" layer="21"/>
<rectangle x1="3.985" y1="1.115" x2="5.375" y2="1.125" layer="21"/>
<rectangle x1="7.265" y1="1.115" x2="7.605" y2="1.125" layer="21"/>
<rectangle x1="10.715" y1="1.115" x2="12.165" y2="1.125" layer="21"/>
<rectangle x1="0.235" y1="1.125" x2="1.655" y2="1.135" layer="21"/>
<rectangle x1="3.985" y1="1.125" x2="5.375" y2="1.135" layer="21"/>
<rectangle x1="7.265" y1="1.125" x2="7.555" y2="1.135" layer="21"/>
<rectangle x1="10.725" y1="1.125" x2="12.165" y2="1.135" layer="21"/>
<rectangle x1="0.235" y1="1.135" x2="1.655" y2="1.145" layer="21"/>
<rectangle x1="3.985" y1="1.135" x2="5.375" y2="1.145" layer="21"/>
<rectangle x1="7.265" y1="1.135" x2="7.515" y2="1.145" layer="21"/>
<rectangle x1="10.735" y1="1.135" x2="12.175" y2="1.145" layer="21"/>
<rectangle x1="0.235" y1="1.145" x2="1.655" y2="1.155" layer="21"/>
<rectangle x1="3.985" y1="1.145" x2="5.375" y2="1.155" layer="21"/>
<rectangle x1="7.255" y1="1.145" x2="7.475" y2="1.155" layer="21"/>
<rectangle x1="10.745" y1="1.145" x2="12.175" y2="1.155" layer="21"/>
<rectangle x1="0.235" y1="1.155" x2="1.655" y2="1.165" layer="21"/>
<rectangle x1="3.985" y1="1.155" x2="5.375" y2="1.165" layer="21"/>
<rectangle x1="7.255" y1="1.155" x2="7.435" y2="1.165" layer="21"/>
<rectangle x1="10.755" y1="1.155" x2="12.185" y2="1.165" layer="21"/>
<rectangle x1="0.235" y1="1.165" x2="1.655" y2="1.175" layer="21"/>
<rectangle x1="3.985" y1="1.165" x2="5.375" y2="1.175" layer="21"/>
<rectangle x1="7.255" y1="1.165" x2="7.395" y2="1.175" layer="21"/>
<rectangle x1="10.775" y1="1.165" x2="12.185" y2="1.175" layer="21"/>
<rectangle x1="0.235" y1="1.175" x2="1.655" y2="1.185" layer="21"/>
<rectangle x1="3.985" y1="1.175" x2="5.375" y2="1.185" layer="21"/>
<rectangle x1="7.255" y1="1.175" x2="7.355" y2="1.185" layer="21"/>
<rectangle x1="10.785" y1="1.175" x2="12.185" y2="1.185" layer="21"/>
<rectangle x1="0.235" y1="1.185" x2="1.655" y2="1.195" layer="21"/>
<rectangle x1="3.985" y1="1.185" x2="5.375" y2="1.195" layer="21"/>
<rectangle x1="7.255" y1="1.185" x2="7.305" y2="1.195" layer="21"/>
<rectangle x1="10.785" y1="1.185" x2="12.195" y2="1.195" layer="21"/>
<rectangle x1="0.235" y1="1.195" x2="1.655" y2="1.205" layer="21"/>
<rectangle x1="3.985" y1="1.195" x2="5.375" y2="1.205" layer="21"/>
<rectangle x1="10.795" y1="1.195" x2="12.195" y2="1.205" layer="21"/>
<rectangle x1="0.235" y1="1.205" x2="1.655" y2="1.215" layer="21"/>
<rectangle x1="3.985" y1="1.205" x2="5.375" y2="1.215" layer="21"/>
<rectangle x1="10.805" y1="1.205" x2="12.205" y2="1.215" layer="21"/>
<rectangle x1="0.235" y1="1.215" x2="1.655" y2="1.225" layer="21"/>
<rectangle x1="3.985" y1="1.215" x2="5.375" y2="1.225" layer="21"/>
<rectangle x1="10.815" y1="1.215" x2="12.205" y2="1.225" layer="21"/>
<rectangle x1="0.235" y1="1.225" x2="1.655" y2="1.235" layer="21"/>
<rectangle x1="3.985" y1="1.225" x2="5.375" y2="1.235" layer="21"/>
<rectangle x1="10.825" y1="1.225" x2="12.205" y2="1.235" layer="21"/>
<rectangle x1="0.235" y1="1.235" x2="1.655" y2="1.245" layer="21"/>
<rectangle x1="3.985" y1="1.235" x2="5.375" y2="1.245" layer="21"/>
<rectangle x1="10.835" y1="1.235" x2="12.205" y2="1.245" layer="21"/>
<rectangle x1="0.235" y1="1.245" x2="1.655" y2="1.255" layer="21"/>
<rectangle x1="3.985" y1="1.245" x2="5.375" y2="1.255" layer="21"/>
<rectangle x1="10.835" y1="1.245" x2="12.215" y2="1.255" layer="21"/>
<rectangle x1="0.235" y1="1.255" x2="1.655" y2="1.265" layer="21"/>
<rectangle x1="3.985" y1="1.255" x2="5.375" y2="1.265" layer="21"/>
<rectangle x1="10.845" y1="1.255" x2="12.215" y2="1.265" layer="21"/>
<rectangle x1="0.235" y1="1.265" x2="1.655" y2="1.275" layer="21"/>
<rectangle x1="3.985" y1="1.265" x2="5.375" y2="1.275" layer="21"/>
<rectangle x1="10.855" y1="1.265" x2="12.215" y2="1.275" layer="21"/>
<rectangle x1="0.235" y1="1.275" x2="1.655" y2="1.285" layer="21"/>
<rectangle x1="3.985" y1="1.275" x2="5.375" y2="1.285" layer="21"/>
<rectangle x1="10.855" y1="1.275" x2="12.215" y2="1.285" layer="21"/>
<rectangle x1="0.235" y1="1.285" x2="1.655" y2="1.295" layer="21"/>
<rectangle x1="3.985" y1="1.285" x2="5.375" y2="1.295" layer="21"/>
<rectangle x1="10.865" y1="1.285" x2="12.215" y2="1.295" layer="21"/>
<rectangle x1="0.235" y1="1.295" x2="1.655" y2="1.305" layer="21"/>
<rectangle x1="3.985" y1="1.295" x2="5.375" y2="1.305" layer="21"/>
<rectangle x1="10.865" y1="1.295" x2="12.225" y2="1.305" layer="21"/>
<rectangle x1="0.235" y1="1.305" x2="1.655" y2="1.315" layer="21"/>
<rectangle x1="3.985" y1="1.305" x2="5.375" y2="1.315" layer="21"/>
<rectangle x1="10.875" y1="1.305" x2="12.225" y2="1.315" layer="21"/>
<rectangle x1="0.235" y1="1.315" x2="1.655" y2="1.325" layer="21"/>
<rectangle x1="3.985" y1="1.315" x2="5.375" y2="1.325" layer="21"/>
<rectangle x1="10.875" y1="1.315" x2="12.225" y2="1.325" layer="21"/>
<rectangle x1="0.235" y1="1.325" x2="1.655" y2="1.335" layer="21"/>
<rectangle x1="3.985" y1="1.325" x2="5.375" y2="1.335" layer="21"/>
<rectangle x1="10.885" y1="1.325" x2="12.225" y2="1.335" layer="21"/>
<rectangle x1="0.235" y1="1.335" x2="1.655" y2="1.345" layer="21"/>
<rectangle x1="3.985" y1="1.335" x2="5.375" y2="1.345" layer="21"/>
<rectangle x1="10.885" y1="1.335" x2="12.225" y2="1.345" layer="21"/>
<rectangle x1="0.235" y1="1.345" x2="1.655" y2="1.355" layer="21"/>
<rectangle x1="3.985" y1="1.345" x2="5.375" y2="1.355" layer="21"/>
<rectangle x1="10.895" y1="1.345" x2="12.225" y2="1.355" layer="21"/>
<rectangle x1="0.235" y1="1.355" x2="1.655" y2="1.365" layer="21"/>
<rectangle x1="3.985" y1="1.355" x2="5.375" y2="1.365" layer="21"/>
<rectangle x1="10.895" y1="1.355" x2="12.225" y2="1.365" layer="21"/>
<rectangle x1="0.235" y1="1.365" x2="1.655" y2="1.375" layer="21"/>
<rectangle x1="3.985" y1="1.365" x2="5.375" y2="1.375" layer="21"/>
<rectangle x1="10.905" y1="1.365" x2="12.225" y2="1.375" layer="21"/>
<rectangle x1="0.235" y1="1.375" x2="1.655" y2="1.385" layer="21"/>
<rectangle x1="3.985" y1="1.375" x2="5.375" y2="1.385" layer="21"/>
<rectangle x1="10.905" y1="1.375" x2="12.225" y2="1.385" layer="21"/>
<rectangle x1="0.235" y1="1.385" x2="1.655" y2="1.395" layer="21"/>
<rectangle x1="3.985" y1="1.385" x2="5.375" y2="1.395" layer="21"/>
<rectangle x1="10.905" y1="1.385" x2="12.225" y2="1.395" layer="21"/>
<rectangle x1="0.235" y1="1.395" x2="1.655" y2="1.405" layer="21"/>
<rectangle x1="3.985" y1="1.395" x2="5.375" y2="1.405" layer="21"/>
<rectangle x1="10.915" y1="1.395" x2="12.225" y2="1.405" layer="21"/>
<rectangle x1="0.235" y1="1.405" x2="1.655" y2="1.415" layer="21"/>
<rectangle x1="3.985" y1="1.405" x2="5.385" y2="1.415" layer="21"/>
<rectangle x1="10.915" y1="1.405" x2="12.225" y2="1.415" layer="21"/>
<rectangle x1="0.235" y1="1.415" x2="1.655" y2="1.425" layer="21"/>
<rectangle x1="3.985" y1="1.415" x2="5.385" y2="1.425" layer="21"/>
<rectangle x1="10.915" y1="1.415" x2="12.235" y2="1.425" layer="21"/>
<rectangle x1="0.235" y1="1.425" x2="1.655" y2="1.435" layer="21"/>
<rectangle x1="3.985" y1="1.425" x2="5.385" y2="1.435" layer="21"/>
<rectangle x1="10.915" y1="1.425" x2="12.235" y2="1.435" layer="21"/>
<rectangle x1="0.235" y1="1.435" x2="1.655" y2="1.445" layer="21"/>
<rectangle x1="3.985" y1="1.435" x2="5.385" y2="1.445" layer="21"/>
<rectangle x1="10.925" y1="1.435" x2="12.235" y2="1.445" layer="21"/>
<rectangle x1="0.235" y1="1.445" x2="1.655" y2="1.455" layer="21"/>
<rectangle x1="3.985" y1="1.445" x2="5.385" y2="1.455" layer="21"/>
<rectangle x1="10.925" y1="1.445" x2="12.235" y2="1.455" layer="21"/>
<rectangle x1="0.235" y1="1.455" x2="1.655" y2="1.465" layer="21"/>
<rectangle x1="3.985" y1="1.455" x2="5.385" y2="1.465" layer="21"/>
<rectangle x1="10.925" y1="1.455" x2="12.235" y2="1.465" layer="21"/>
<rectangle x1="0.235" y1="1.465" x2="1.655" y2="1.475" layer="21"/>
<rectangle x1="3.985" y1="1.465" x2="5.385" y2="1.475" layer="21"/>
<rectangle x1="10.925" y1="1.465" x2="12.235" y2="1.475" layer="21"/>
<rectangle x1="0.235" y1="1.475" x2="1.655" y2="1.485" layer="21"/>
<rectangle x1="3.985" y1="1.475" x2="5.385" y2="1.485" layer="21"/>
<rectangle x1="10.925" y1="1.475" x2="12.235" y2="1.485" layer="21"/>
<rectangle x1="0.235" y1="1.485" x2="1.655" y2="1.495" layer="21"/>
<rectangle x1="3.985" y1="1.485" x2="5.385" y2="1.495" layer="21"/>
<rectangle x1="10.925" y1="1.485" x2="12.235" y2="1.495" layer="21"/>
<rectangle x1="0.235" y1="1.495" x2="1.655" y2="1.505" layer="21"/>
<rectangle x1="3.985" y1="1.495" x2="5.385" y2="1.505" layer="21"/>
<rectangle x1="10.925" y1="1.495" x2="12.235" y2="1.505" layer="21"/>
<rectangle x1="0.235" y1="1.505" x2="1.655" y2="1.515" layer="21"/>
<rectangle x1="3.985" y1="1.505" x2="5.385" y2="1.515" layer="21"/>
<rectangle x1="10.935" y1="1.505" x2="12.235" y2="1.515" layer="21"/>
<rectangle x1="0.235" y1="1.515" x2="1.655" y2="1.525" layer="21"/>
<rectangle x1="3.985" y1="1.515" x2="5.385" y2="1.525" layer="21"/>
<rectangle x1="10.935" y1="1.515" x2="12.235" y2="1.525" layer="21"/>
<rectangle x1="0.235" y1="1.525" x2="1.655" y2="1.535" layer="21"/>
<rectangle x1="3.985" y1="1.525" x2="5.385" y2="1.535" layer="21"/>
<rectangle x1="10.935" y1="1.525" x2="12.235" y2="1.535" layer="21"/>
<rectangle x1="0.235" y1="1.535" x2="1.655" y2="1.545" layer="21"/>
<rectangle x1="3.995" y1="1.535" x2="5.385" y2="1.545" layer="21"/>
<rectangle x1="10.935" y1="1.535" x2="12.235" y2="1.545" layer="21"/>
<rectangle x1="0.235" y1="1.545" x2="1.655" y2="1.555" layer="21"/>
<rectangle x1="3.995" y1="1.545" x2="5.385" y2="1.555" layer="21"/>
<rectangle x1="10.935" y1="1.545" x2="12.235" y2="1.555" layer="21"/>
<rectangle x1="0.235" y1="1.555" x2="1.655" y2="1.565" layer="21"/>
<rectangle x1="3.995" y1="1.555" x2="5.385" y2="1.565" layer="21"/>
<rectangle x1="10.935" y1="1.555" x2="12.235" y2="1.565" layer="21"/>
<rectangle x1="0.235" y1="1.565" x2="1.655" y2="1.575" layer="21"/>
<rectangle x1="3.995" y1="1.565" x2="5.385" y2="1.575" layer="21"/>
<rectangle x1="10.935" y1="1.565" x2="12.235" y2="1.575" layer="21"/>
<rectangle x1="0.235" y1="1.575" x2="1.655" y2="1.585" layer="21"/>
<rectangle x1="3.995" y1="1.575" x2="5.385" y2="1.585" layer="21"/>
<rectangle x1="10.935" y1="1.575" x2="12.235" y2="1.585" layer="21"/>
<rectangle x1="0.235" y1="1.585" x2="1.655" y2="1.595" layer="21"/>
<rectangle x1="3.995" y1="1.585" x2="5.385" y2="1.595" layer="21"/>
<rectangle x1="10.935" y1="1.585" x2="12.235" y2="1.595" layer="21"/>
<rectangle x1="0.235" y1="1.595" x2="1.655" y2="1.605" layer="21"/>
<rectangle x1="3.995" y1="1.595" x2="5.385" y2="1.605" layer="21"/>
<rectangle x1="10.925" y1="1.595" x2="12.235" y2="1.605" layer="21"/>
<rectangle x1="0.235" y1="1.605" x2="1.655" y2="1.615" layer="21"/>
<rectangle x1="3.995" y1="1.605" x2="5.385" y2="1.615" layer="21"/>
<rectangle x1="10.925" y1="1.605" x2="12.235" y2="1.615" layer="21"/>
<rectangle x1="0.235" y1="1.615" x2="1.655" y2="1.625" layer="21"/>
<rectangle x1="3.995" y1="1.615" x2="5.385" y2="1.625" layer="21"/>
<rectangle x1="10.925" y1="1.615" x2="12.235" y2="1.625" layer="21"/>
<rectangle x1="0.235" y1="1.625" x2="1.655" y2="1.635" layer="21"/>
<rectangle x1="3.995" y1="1.625" x2="5.385" y2="1.635" layer="21"/>
<rectangle x1="10.925" y1="1.625" x2="12.235" y2="1.635" layer="21"/>
<rectangle x1="0.235" y1="1.635" x2="1.655" y2="1.645" layer="21"/>
<rectangle x1="3.995" y1="1.635" x2="5.385" y2="1.645" layer="21"/>
<rectangle x1="10.925" y1="1.635" x2="12.235" y2="1.645" layer="21"/>
<rectangle x1="0.235" y1="1.645" x2="1.655" y2="1.655" layer="21"/>
<rectangle x1="3.995" y1="1.645" x2="5.385" y2="1.655" layer="21"/>
<rectangle x1="10.925" y1="1.645" x2="12.235" y2="1.655" layer="21"/>
<rectangle x1="0.235" y1="1.655" x2="1.655" y2="1.665" layer="21"/>
<rectangle x1="3.995" y1="1.655" x2="5.385" y2="1.665" layer="21"/>
<rectangle x1="10.915" y1="1.655" x2="12.235" y2="1.665" layer="21"/>
<rectangle x1="0.235" y1="1.665" x2="1.655" y2="1.675" layer="21"/>
<rectangle x1="3.995" y1="1.665" x2="5.385" y2="1.675" layer="21"/>
<rectangle x1="10.915" y1="1.665" x2="12.235" y2="1.675" layer="21"/>
<rectangle x1="0.235" y1="1.675" x2="1.655" y2="1.685" layer="21"/>
<rectangle x1="3.995" y1="1.675" x2="5.385" y2="1.685" layer="21"/>
<rectangle x1="10.915" y1="1.675" x2="12.235" y2="1.685" layer="21"/>
<rectangle x1="0.235" y1="1.685" x2="1.655" y2="1.695" layer="21"/>
<rectangle x1="3.995" y1="1.685" x2="5.385" y2="1.695" layer="21"/>
<rectangle x1="10.915" y1="1.685" x2="12.235" y2="1.695" layer="21"/>
<rectangle x1="0.235" y1="1.695" x2="1.655" y2="1.705" layer="21"/>
<rectangle x1="3.995" y1="1.695" x2="5.385" y2="1.705" layer="21"/>
<rectangle x1="10.905" y1="1.695" x2="12.235" y2="1.705" layer="21"/>
<rectangle x1="0.235" y1="1.705" x2="1.655" y2="1.715" layer="21"/>
<rectangle x1="3.995" y1="1.705" x2="5.385" y2="1.715" layer="21"/>
<rectangle x1="10.905" y1="1.705" x2="12.235" y2="1.715" layer="21"/>
<rectangle x1="0.235" y1="1.715" x2="1.655" y2="1.725" layer="21"/>
<rectangle x1="3.995" y1="1.715" x2="5.385" y2="1.725" layer="21"/>
<rectangle x1="10.905" y1="1.715" x2="12.235" y2="1.725" layer="21"/>
<rectangle x1="0.235" y1="1.725" x2="1.655" y2="1.735" layer="21"/>
<rectangle x1="3.995" y1="1.725" x2="5.385" y2="1.735" layer="21"/>
<rectangle x1="10.895" y1="1.725" x2="12.235" y2="1.735" layer="21"/>
<rectangle x1="0.235" y1="1.735" x2="1.655" y2="1.745" layer="21"/>
<rectangle x1="3.995" y1="1.735" x2="5.385" y2="1.745" layer="21"/>
<rectangle x1="10.895" y1="1.735" x2="12.235" y2="1.745" layer="21"/>
<rectangle x1="0.235" y1="1.745" x2="1.655" y2="1.755" layer="21"/>
<rectangle x1="3.995" y1="1.745" x2="5.385" y2="1.755" layer="21"/>
<rectangle x1="10.885" y1="1.745" x2="12.235" y2="1.755" layer="21"/>
<rectangle x1="0.235" y1="1.755" x2="1.655" y2="1.765" layer="21"/>
<rectangle x1="3.995" y1="1.755" x2="5.385" y2="1.765" layer="21"/>
<rectangle x1="10.885" y1="1.755" x2="12.235" y2="1.765" layer="21"/>
<rectangle x1="0.235" y1="1.765" x2="1.655" y2="1.775" layer="21"/>
<rectangle x1="3.995" y1="1.765" x2="5.385" y2="1.775" layer="21"/>
<rectangle x1="10.875" y1="1.765" x2="12.235" y2="1.775" layer="21"/>
<rectangle x1="0.235" y1="1.775" x2="1.655" y2="1.785" layer="21"/>
<rectangle x1="3.995" y1="1.775" x2="5.385" y2="1.785" layer="21"/>
<rectangle x1="10.875" y1="1.775" x2="12.235" y2="1.785" layer="21"/>
<rectangle x1="0.235" y1="1.785" x2="1.655" y2="1.795" layer="21"/>
<rectangle x1="3.995" y1="1.785" x2="5.385" y2="1.795" layer="21"/>
<rectangle x1="10.865" y1="1.785" x2="12.235" y2="1.795" layer="21"/>
<rectangle x1="0.235" y1="1.795" x2="1.655" y2="1.805" layer="21"/>
<rectangle x1="3.995" y1="1.795" x2="5.385" y2="1.805" layer="21"/>
<rectangle x1="10.865" y1="1.795" x2="12.235" y2="1.805" layer="21"/>
<rectangle x1="0.235" y1="1.805" x2="1.655" y2="1.815" layer="21"/>
<rectangle x1="3.995" y1="1.805" x2="5.385" y2="1.815" layer="21"/>
<rectangle x1="10.855" y1="1.805" x2="12.235" y2="1.815" layer="21"/>
<rectangle x1="0.235" y1="1.815" x2="1.655" y2="1.825" layer="21"/>
<rectangle x1="3.995" y1="1.815" x2="5.385" y2="1.825" layer="21"/>
<rectangle x1="10.845" y1="1.815" x2="12.235" y2="1.825" layer="21"/>
<rectangle x1="0.235" y1="1.825" x2="1.655" y2="1.835" layer="21"/>
<rectangle x1="3.995" y1="1.825" x2="5.385" y2="1.835" layer="21"/>
<rectangle x1="10.835" y1="1.825" x2="12.235" y2="1.835" layer="21"/>
<rectangle x1="0.235" y1="1.835" x2="1.655" y2="1.845" layer="21"/>
<rectangle x1="3.995" y1="1.835" x2="5.385" y2="1.845" layer="21"/>
<rectangle x1="10.835" y1="1.835" x2="12.235" y2="1.845" layer="21"/>
<rectangle x1="0.235" y1="1.845" x2="1.655" y2="1.855" layer="21"/>
<rectangle x1="3.995" y1="1.845" x2="5.385" y2="1.855" layer="21"/>
<rectangle x1="10.825" y1="1.845" x2="12.235" y2="1.855" layer="21"/>
<rectangle x1="0.235" y1="1.855" x2="1.655" y2="1.865" layer="21"/>
<rectangle x1="3.995" y1="1.855" x2="5.385" y2="1.865" layer="21"/>
<rectangle x1="10.815" y1="1.855" x2="12.235" y2="1.865" layer="21"/>
<rectangle x1="0.235" y1="1.865" x2="1.655" y2="1.875" layer="21"/>
<rectangle x1="3.995" y1="1.865" x2="5.385" y2="1.875" layer="21"/>
<rectangle x1="10.805" y1="1.865" x2="12.225" y2="1.875" layer="21"/>
<rectangle x1="0.235" y1="1.875" x2="1.655" y2="1.885" layer="21"/>
<rectangle x1="3.995" y1="1.875" x2="5.385" y2="1.885" layer="21"/>
<rectangle x1="10.795" y1="1.875" x2="12.225" y2="1.885" layer="21"/>
<rectangle x1="0.235" y1="1.885" x2="1.655" y2="1.895" layer="21"/>
<rectangle x1="3.995" y1="1.885" x2="5.385" y2="1.895" layer="21"/>
<rectangle x1="10.785" y1="1.885" x2="12.225" y2="1.895" layer="21"/>
<rectangle x1="0.235" y1="1.895" x2="1.655" y2="1.905" layer="21"/>
<rectangle x1="3.995" y1="1.895" x2="5.385" y2="1.905" layer="21"/>
<rectangle x1="10.775" y1="1.895" x2="12.225" y2="1.905" layer="21"/>
<rectangle x1="0.235" y1="1.905" x2="1.655" y2="1.915" layer="21"/>
<rectangle x1="3.995" y1="1.905" x2="5.385" y2="1.915" layer="21"/>
<rectangle x1="10.765" y1="1.905" x2="12.225" y2="1.915" layer="21"/>
<rectangle x1="0.235" y1="1.915" x2="1.655" y2="1.925" layer="21"/>
<rectangle x1="3.995" y1="1.915" x2="5.385" y2="1.925" layer="21"/>
<rectangle x1="10.755" y1="1.915" x2="12.225" y2="1.925" layer="21"/>
<rectangle x1="0.235" y1="1.925" x2="1.655" y2="1.935" layer="21"/>
<rectangle x1="3.995" y1="1.925" x2="5.385" y2="1.935" layer="21"/>
<rectangle x1="10.745" y1="1.925" x2="12.225" y2="1.935" layer="21"/>
<rectangle x1="0.235" y1="1.935" x2="1.655" y2="1.945" layer="21"/>
<rectangle x1="3.995" y1="1.935" x2="5.385" y2="1.945" layer="21"/>
<rectangle x1="10.735" y1="1.935" x2="12.225" y2="1.945" layer="21"/>
<rectangle x1="0.235" y1="1.945" x2="1.655" y2="1.955" layer="21"/>
<rectangle x1="3.995" y1="1.945" x2="5.385" y2="1.955" layer="21"/>
<rectangle x1="10.725" y1="1.945" x2="12.225" y2="1.955" layer="21"/>
<rectangle x1="0.235" y1="1.955" x2="1.655" y2="1.965" layer="21"/>
<rectangle x1="3.995" y1="1.955" x2="5.385" y2="1.965" layer="21"/>
<rectangle x1="10.705" y1="1.955" x2="12.225" y2="1.965" layer="21"/>
<rectangle x1="0.235" y1="1.965" x2="1.655" y2="1.975" layer="21"/>
<rectangle x1="3.995" y1="1.965" x2="5.385" y2="1.975" layer="21"/>
<rectangle x1="10.695" y1="1.965" x2="12.225" y2="1.975" layer="21"/>
<rectangle x1="0.235" y1="1.975" x2="1.655" y2="1.985" layer="21"/>
<rectangle x1="3.995" y1="1.975" x2="5.385" y2="1.985" layer="21"/>
<rectangle x1="10.685" y1="1.975" x2="12.225" y2="1.985" layer="21"/>
<rectangle x1="0.235" y1="1.985" x2="1.655" y2="1.995" layer="21"/>
<rectangle x1="3.995" y1="1.985" x2="5.385" y2="1.995" layer="21"/>
<rectangle x1="10.665" y1="1.985" x2="12.225" y2="1.995" layer="21"/>
<rectangle x1="0.235" y1="1.995" x2="1.655" y2="2.005" layer="21"/>
<rectangle x1="3.995" y1="1.995" x2="5.385" y2="2.005" layer="21"/>
<rectangle x1="10.655" y1="1.995" x2="12.225" y2="2.005" layer="21"/>
<rectangle x1="0.235" y1="2.005" x2="1.655" y2="2.015" layer="21"/>
<rectangle x1="3.995" y1="2.005" x2="5.385" y2="2.015" layer="21"/>
<rectangle x1="10.635" y1="2.005" x2="12.215" y2="2.015" layer="21"/>
<rectangle x1="0.235" y1="2.015" x2="1.655" y2="2.025" layer="21"/>
<rectangle x1="3.995" y1="2.015" x2="5.385" y2="2.025" layer="21"/>
<rectangle x1="10.625" y1="2.015" x2="12.215" y2="2.025" layer="21"/>
<rectangle x1="0.235" y1="2.025" x2="1.655" y2="2.035" layer="21"/>
<rectangle x1="3.995" y1="2.025" x2="5.385" y2="2.035" layer="21"/>
<rectangle x1="10.605" y1="2.025" x2="12.215" y2="2.035" layer="21"/>
<rectangle x1="0.235" y1="2.035" x2="1.655" y2="2.045" layer="21"/>
<rectangle x1="3.995" y1="2.035" x2="5.385" y2="2.045" layer="21"/>
<rectangle x1="10.585" y1="2.035" x2="12.215" y2="2.045" layer="21"/>
<rectangle x1="0.235" y1="2.045" x2="1.655" y2="2.055" layer="21"/>
<rectangle x1="3.995" y1="2.045" x2="5.385" y2="2.055" layer="21"/>
<rectangle x1="10.565" y1="2.045" x2="12.215" y2="2.055" layer="21"/>
<rectangle x1="0.235" y1="2.055" x2="1.655" y2="2.065" layer="21"/>
<rectangle x1="3.995" y1="2.055" x2="5.385" y2="2.065" layer="21"/>
<rectangle x1="10.545" y1="2.055" x2="12.205" y2="2.065" layer="21"/>
<rectangle x1="0.235" y1="2.065" x2="1.655" y2="2.075" layer="21"/>
<rectangle x1="3.995" y1="2.065" x2="5.385" y2="2.075" layer="21"/>
<rectangle x1="10.535" y1="2.065" x2="12.205" y2="2.075" layer="21"/>
<rectangle x1="0.235" y1="2.075" x2="1.655" y2="2.085" layer="21"/>
<rectangle x1="3.995" y1="2.075" x2="5.385" y2="2.085" layer="21"/>
<rectangle x1="10.515" y1="2.075" x2="12.205" y2="2.085" layer="21"/>
<rectangle x1="0.235" y1="2.085" x2="1.655" y2="2.095" layer="21"/>
<rectangle x1="3.995" y1="2.085" x2="5.385" y2="2.095" layer="21"/>
<rectangle x1="10.485" y1="2.085" x2="12.205" y2="2.095" layer="21"/>
<rectangle x1="0.235" y1="2.095" x2="1.655" y2="2.105" layer="21"/>
<rectangle x1="3.995" y1="2.095" x2="5.385" y2="2.105" layer="21"/>
<rectangle x1="10.465" y1="2.095" x2="12.195" y2="2.105" layer="21"/>
<rectangle x1="0.235" y1="2.105" x2="1.655" y2="2.115" layer="21"/>
<rectangle x1="3.995" y1="2.105" x2="5.385" y2="2.115" layer="21"/>
<rectangle x1="10.445" y1="2.105" x2="12.195" y2="2.115" layer="21"/>
<rectangle x1="0.235" y1="2.115" x2="1.655" y2="2.125" layer="21"/>
<rectangle x1="3.995" y1="2.115" x2="5.385" y2="2.125" layer="21"/>
<rectangle x1="10.425" y1="2.115" x2="12.195" y2="2.125" layer="21"/>
<rectangle x1="0.235" y1="2.125" x2="1.655" y2="2.135" layer="21"/>
<rectangle x1="3.995" y1="2.125" x2="5.385" y2="2.135" layer="21"/>
<rectangle x1="10.395" y1="2.125" x2="12.185" y2="2.135" layer="21"/>
<rectangle x1="0.235" y1="2.135" x2="1.655" y2="2.145" layer="21"/>
<rectangle x1="3.995" y1="2.135" x2="5.385" y2="2.145" layer="21"/>
<rectangle x1="10.375" y1="2.135" x2="12.185" y2="2.145" layer="21"/>
<rectangle x1="0.235" y1="2.145" x2="1.655" y2="2.155" layer="21"/>
<rectangle x1="3.995" y1="2.145" x2="5.385" y2="2.155" layer="21"/>
<rectangle x1="10.345" y1="2.145" x2="12.175" y2="2.155" layer="21"/>
<rectangle x1="0.235" y1="2.155" x2="1.655" y2="2.165" layer="21"/>
<rectangle x1="3.995" y1="2.155" x2="5.385" y2="2.165" layer="21"/>
<rectangle x1="10.315" y1="2.155" x2="12.175" y2="2.165" layer="21"/>
<rectangle x1="0.235" y1="2.165" x2="1.655" y2="2.175" layer="21"/>
<rectangle x1="3.995" y1="2.165" x2="5.385" y2="2.175" layer="21"/>
<rectangle x1="10.285" y1="2.165" x2="12.165" y2="2.175" layer="21"/>
<rectangle x1="0.235" y1="2.175" x2="1.655" y2="2.185" layer="21"/>
<rectangle x1="3.995" y1="2.175" x2="5.385" y2="2.185" layer="21"/>
<rectangle x1="10.255" y1="2.175" x2="12.165" y2="2.185" layer="21"/>
<rectangle x1="0.235" y1="2.185" x2="1.655" y2="2.195" layer="21"/>
<rectangle x1="3.995" y1="2.185" x2="5.385" y2="2.195" layer="21"/>
<rectangle x1="10.225" y1="2.185" x2="12.155" y2="2.195" layer="21"/>
<rectangle x1="0.235" y1="2.195" x2="1.655" y2="2.205" layer="21"/>
<rectangle x1="3.995" y1="2.195" x2="5.385" y2="2.205" layer="21"/>
<rectangle x1="10.185" y1="2.195" x2="12.155" y2="2.205" layer="21"/>
<rectangle x1="0.235" y1="2.205" x2="1.655" y2="2.215" layer="21"/>
<rectangle x1="3.995" y1="2.205" x2="5.385" y2="2.215" layer="21"/>
<rectangle x1="10.155" y1="2.205" x2="12.145" y2="2.215" layer="21"/>
<rectangle x1="0.245" y1="2.215" x2="1.655" y2="2.225" layer="21"/>
<rectangle x1="3.995" y1="2.215" x2="5.385" y2="2.225" layer="21"/>
<rectangle x1="10.115" y1="2.215" x2="12.145" y2="2.225" layer="21"/>
<rectangle x1="0.245" y1="2.225" x2="1.655" y2="2.235" layer="21"/>
<rectangle x1="3.995" y1="2.225" x2="5.385" y2="2.235" layer="21"/>
<rectangle x1="10.085" y1="2.225" x2="12.135" y2="2.235" layer="21"/>
<rectangle x1="0.245" y1="2.235" x2="1.655" y2="2.245" layer="21"/>
<rectangle x1="3.995" y1="2.235" x2="5.385" y2="2.245" layer="21"/>
<rectangle x1="10.045" y1="2.235" x2="12.135" y2="2.245" layer="21"/>
<rectangle x1="0.245" y1="2.245" x2="1.655" y2="2.255" layer="21"/>
<rectangle x1="3.995" y1="2.245" x2="5.385" y2="2.255" layer="21"/>
<rectangle x1="10.005" y1="2.245" x2="12.125" y2="2.255" layer="21"/>
<rectangle x1="0.245" y1="2.255" x2="1.655" y2="2.265" layer="21"/>
<rectangle x1="3.995" y1="2.255" x2="5.385" y2="2.265" layer="21"/>
<rectangle x1="9.965" y1="2.255" x2="12.125" y2="2.265" layer="21"/>
<rectangle x1="0.245" y1="2.265" x2="1.655" y2="2.275" layer="21"/>
<rectangle x1="3.995" y1="2.265" x2="5.385" y2="2.275" layer="21"/>
<rectangle x1="9.925" y1="2.265" x2="12.115" y2="2.275" layer="21"/>
<rectangle x1="0.245" y1="2.275" x2="1.655" y2="2.285" layer="21"/>
<rectangle x1="3.995" y1="2.275" x2="5.395" y2="2.285" layer="21"/>
<rectangle x1="9.875" y1="2.275" x2="12.115" y2="2.285" layer="21"/>
<rectangle x1="0.245" y1="2.285" x2="1.655" y2="2.295" layer="21"/>
<rectangle x1="3.995" y1="2.285" x2="5.395" y2="2.295" layer="21"/>
<rectangle x1="9.835" y1="2.285" x2="12.105" y2="2.295" layer="21"/>
<rectangle x1="0.245" y1="2.295" x2="1.655" y2="2.305" layer="21"/>
<rectangle x1="3.995" y1="2.295" x2="5.395" y2="2.305" layer="21"/>
<rectangle x1="9.785" y1="2.295" x2="12.105" y2="2.305" layer="21"/>
<rectangle x1="0.245" y1="2.305" x2="1.655" y2="2.315" layer="21"/>
<rectangle x1="3.995" y1="2.305" x2="5.395" y2="2.315" layer="21"/>
<rectangle x1="9.745" y1="2.305" x2="12.095" y2="2.315" layer="21"/>
<rectangle x1="0.245" y1="2.315" x2="1.655" y2="2.325" layer="21"/>
<rectangle x1="3.995" y1="2.315" x2="5.395" y2="2.325" layer="21"/>
<rectangle x1="9.695" y1="2.315" x2="12.085" y2="2.325" layer="21"/>
<rectangle x1="0.245" y1="2.325" x2="1.655" y2="2.335" layer="21"/>
<rectangle x1="3.995" y1="2.325" x2="5.395" y2="2.335" layer="21"/>
<rectangle x1="9.645" y1="2.325" x2="12.085" y2="2.335" layer="21"/>
<rectangle x1="0.245" y1="2.335" x2="1.655" y2="2.345" layer="21"/>
<rectangle x1="3.995" y1="2.335" x2="5.395" y2="2.345" layer="21"/>
<rectangle x1="9.605" y1="2.335" x2="12.075" y2="2.345" layer="21"/>
<rectangle x1="0.245" y1="2.345" x2="1.655" y2="2.355" layer="21"/>
<rectangle x1="3.995" y1="2.345" x2="5.395" y2="2.355" layer="21"/>
<rectangle x1="9.545" y1="2.345" x2="12.075" y2="2.355" layer="21"/>
<rectangle x1="0.245" y1="2.355" x2="1.655" y2="2.365" layer="21"/>
<rectangle x1="3.995" y1="2.355" x2="5.395" y2="2.365" layer="21"/>
<rectangle x1="9.495" y1="2.355" x2="12.065" y2="2.365" layer="21"/>
<rectangle x1="0.245" y1="2.365" x2="1.655" y2="2.375" layer="21"/>
<rectangle x1="3.995" y1="2.365" x2="5.395" y2="2.375" layer="21"/>
<rectangle x1="9.445" y1="2.365" x2="12.055" y2="2.375" layer="21"/>
<rectangle x1="0.245" y1="2.375" x2="1.655" y2="2.385" layer="21"/>
<rectangle x1="3.995" y1="2.375" x2="5.395" y2="2.385" layer="21"/>
<rectangle x1="9.405" y1="2.375" x2="12.055" y2="2.385" layer="21"/>
<rectangle x1="0.245" y1="2.385" x2="1.655" y2="2.395" layer="21"/>
<rectangle x1="3.995" y1="2.385" x2="5.395" y2="2.395" layer="21"/>
<rectangle x1="9.355" y1="2.385" x2="12.045" y2="2.395" layer="21"/>
<rectangle x1="0.245" y1="2.395" x2="1.655" y2="2.405" layer="21"/>
<rectangle x1="3.995" y1="2.395" x2="5.395" y2="2.405" layer="21"/>
<rectangle x1="9.305" y1="2.395" x2="12.045" y2="2.405" layer="21"/>
<rectangle x1="0.245" y1="2.405" x2="1.655" y2="2.415" layer="21"/>
<rectangle x1="3.995" y1="2.405" x2="5.395" y2="2.415" layer="21"/>
<rectangle x1="9.255" y1="2.405" x2="12.035" y2="2.415" layer="21"/>
<rectangle x1="0.245" y1="2.415" x2="1.655" y2="2.425" layer="21"/>
<rectangle x1="3.995" y1="2.415" x2="5.395" y2="2.425" layer="21"/>
<rectangle x1="9.205" y1="2.415" x2="12.025" y2="2.425" layer="21"/>
<rectangle x1="0.245" y1="2.425" x2="1.655" y2="2.435" layer="21"/>
<rectangle x1="3.995" y1="2.425" x2="5.395" y2="2.435" layer="21"/>
<rectangle x1="9.155" y1="2.425" x2="12.025" y2="2.435" layer="21"/>
<rectangle x1="0.245" y1="2.435" x2="1.655" y2="2.445" layer="21"/>
<rectangle x1="3.995" y1="2.435" x2="5.395" y2="2.445" layer="21"/>
<rectangle x1="9.115" y1="2.435" x2="12.015" y2="2.445" layer="21"/>
<rectangle x1="0.245" y1="2.445" x2="1.655" y2="2.455" layer="21"/>
<rectangle x1="3.995" y1="2.445" x2="5.395" y2="2.455" layer="21"/>
<rectangle x1="9.065" y1="2.445" x2="12.005" y2="2.455" layer="21"/>
<rectangle x1="0.245" y1="2.455" x2="1.655" y2="2.465" layer="21"/>
<rectangle x1="3.995" y1="2.455" x2="5.395" y2="2.465" layer="21"/>
<rectangle x1="9.025" y1="2.455" x2="11.995" y2="2.465" layer="21"/>
<rectangle x1="0.245" y1="2.465" x2="1.655" y2="2.475" layer="21"/>
<rectangle x1="3.995" y1="2.465" x2="5.395" y2="2.475" layer="21"/>
<rectangle x1="8.975" y1="2.465" x2="11.995" y2="2.475" layer="21"/>
<rectangle x1="0.245" y1="2.475" x2="1.655" y2="2.485" layer="21"/>
<rectangle x1="3.995" y1="2.475" x2="5.395" y2="2.485" layer="21"/>
<rectangle x1="8.935" y1="2.475" x2="11.985" y2="2.485" layer="21"/>
<rectangle x1="0.245" y1="2.485" x2="1.655" y2="2.495" layer="21"/>
<rectangle x1="3.995" y1="2.485" x2="5.395" y2="2.495" layer="21"/>
<rectangle x1="8.895" y1="2.485" x2="11.975" y2="2.495" layer="21"/>
<rectangle x1="0.245" y1="2.495" x2="1.655" y2="2.505" layer="21"/>
<rectangle x1="3.995" y1="2.495" x2="5.395" y2="2.505" layer="21"/>
<rectangle x1="8.855" y1="2.495" x2="11.965" y2="2.505" layer="21"/>
<rectangle x1="0.245" y1="2.505" x2="1.655" y2="2.515" layer="21"/>
<rectangle x1="3.995" y1="2.505" x2="5.395" y2="2.515" layer="21"/>
<rectangle x1="8.815" y1="2.505" x2="11.965" y2="2.515" layer="21"/>
<rectangle x1="0.245" y1="2.515" x2="1.655" y2="2.525" layer="21"/>
<rectangle x1="3.995" y1="2.515" x2="5.395" y2="2.525" layer="21"/>
<rectangle x1="8.775" y1="2.515" x2="11.955" y2="2.525" layer="21"/>
<rectangle x1="0.245" y1="2.525" x2="1.655" y2="2.535" layer="21"/>
<rectangle x1="3.995" y1="2.525" x2="5.395" y2="2.535" layer="21"/>
<rectangle x1="8.735" y1="2.525" x2="11.945" y2="2.535" layer="21"/>
<rectangle x1="0.245" y1="2.535" x2="1.655" y2="2.545" layer="21"/>
<rectangle x1="3.995" y1="2.535" x2="5.395" y2="2.545" layer="21"/>
<rectangle x1="8.695" y1="2.535" x2="11.935" y2="2.545" layer="21"/>
<rectangle x1="0.245" y1="2.545" x2="1.655" y2="2.555" layer="21"/>
<rectangle x1="3.995" y1="2.545" x2="5.395" y2="2.555" layer="21"/>
<rectangle x1="8.665" y1="2.545" x2="11.925" y2="2.555" layer="21"/>
<rectangle x1="0.245" y1="2.555" x2="1.655" y2="2.565" layer="21"/>
<rectangle x1="3.995" y1="2.555" x2="5.395" y2="2.565" layer="21"/>
<rectangle x1="8.625" y1="2.555" x2="11.925" y2="2.565" layer="21"/>
<rectangle x1="0.245" y1="2.565" x2="1.655" y2="2.575" layer="21"/>
<rectangle x1="3.995" y1="2.565" x2="5.395" y2="2.575" layer="21"/>
<rectangle x1="8.585" y1="2.565" x2="11.915" y2="2.575" layer="21"/>
<rectangle x1="0.245" y1="2.575" x2="1.655" y2="2.585" layer="21"/>
<rectangle x1="3.995" y1="2.575" x2="5.395" y2="2.585" layer="21"/>
<rectangle x1="8.555" y1="2.575" x2="11.905" y2="2.585" layer="21"/>
<rectangle x1="0.245" y1="2.585" x2="1.655" y2="2.595" layer="21"/>
<rectangle x1="3.995" y1="2.585" x2="5.395" y2="2.595" layer="21"/>
<rectangle x1="8.525" y1="2.585" x2="11.895" y2="2.595" layer="21"/>
<rectangle x1="0.245" y1="2.595" x2="1.655" y2="2.605" layer="21"/>
<rectangle x1="3.995" y1="2.595" x2="5.395" y2="2.605" layer="21"/>
<rectangle x1="8.495" y1="2.595" x2="11.885" y2="2.605" layer="21"/>
<rectangle x1="0.245" y1="2.605" x2="1.655" y2="2.615" layer="21"/>
<rectangle x1="3.995" y1="2.605" x2="5.395" y2="2.615" layer="21"/>
<rectangle x1="8.455" y1="2.605" x2="11.875" y2="2.615" layer="21"/>
<rectangle x1="0.245" y1="2.615" x2="1.655" y2="2.625" layer="21"/>
<rectangle x1="3.995" y1="2.615" x2="5.395" y2="2.625" layer="21"/>
<rectangle x1="8.425" y1="2.615" x2="11.865" y2="2.625" layer="21"/>
<rectangle x1="0.245" y1="2.625" x2="1.655" y2="2.635" layer="21"/>
<rectangle x1="3.995" y1="2.625" x2="5.395" y2="2.635" layer="21"/>
<rectangle x1="8.395" y1="2.625" x2="11.855" y2="2.635" layer="21"/>
<rectangle x1="0.245" y1="2.635" x2="1.655" y2="2.645" layer="21"/>
<rectangle x1="3.995" y1="2.635" x2="5.395" y2="2.645" layer="21"/>
<rectangle x1="8.365" y1="2.635" x2="11.845" y2="2.645" layer="21"/>
<rectangle x1="0.245" y1="2.645" x2="1.655" y2="2.655" layer="21"/>
<rectangle x1="3.995" y1="2.645" x2="5.395" y2="2.655" layer="21"/>
<rectangle x1="8.335" y1="2.645" x2="11.835" y2="2.655" layer="21"/>
<rectangle x1="0.245" y1="2.655" x2="1.655" y2="2.665" layer="21"/>
<rectangle x1="3.995" y1="2.655" x2="5.395" y2="2.665" layer="21"/>
<rectangle x1="8.315" y1="2.655" x2="11.825" y2="2.665" layer="21"/>
<rectangle x1="0.245" y1="2.665" x2="1.655" y2="2.675" layer="21"/>
<rectangle x1="3.995" y1="2.665" x2="5.395" y2="2.675" layer="21"/>
<rectangle x1="8.285" y1="2.665" x2="11.815" y2="2.675" layer="21"/>
<rectangle x1="0.245" y1="2.675" x2="1.655" y2="2.685" layer="21"/>
<rectangle x1="3.995" y1="2.675" x2="5.395" y2="2.685" layer="21"/>
<rectangle x1="8.255" y1="2.675" x2="11.795" y2="2.685" layer="21"/>
<rectangle x1="0.245" y1="2.685" x2="1.655" y2="2.695" layer="21"/>
<rectangle x1="3.995" y1="2.685" x2="5.395" y2="2.695" layer="21"/>
<rectangle x1="8.235" y1="2.685" x2="11.785" y2="2.695" layer="21"/>
<rectangle x1="0.245" y1="2.695" x2="1.655" y2="2.705" layer="21"/>
<rectangle x1="3.995" y1="2.695" x2="5.395" y2="2.705" layer="21"/>
<rectangle x1="8.205" y1="2.695" x2="11.775" y2="2.705" layer="21"/>
<rectangle x1="0.245" y1="2.705" x2="1.655" y2="2.715" layer="21"/>
<rectangle x1="3.995" y1="2.705" x2="5.395" y2="2.715" layer="21"/>
<rectangle x1="8.185" y1="2.705" x2="11.765" y2="2.715" layer="21"/>
<rectangle x1="0.245" y1="2.715" x2="1.655" y2="2.725" layer="21"/>
<rectangle x1="3.995" y1="2.715" x2="5.395" y2="2.725" layer="21"/>
<rectangle x1="8.155" y1="2.715" x2="11.755" y2="2.725" layer="21"/>
<rectangle x1="0.245" y1="2.725" x2="1.655" y2="2.735" layer="21"/>
<rectangle x1="3.995" y1="2.725" x2="5.395" y2="2.735" layer="21"/>
<rectangle x1="8.135" y1="2.725" x2="11.735" y2="2.735" layer="21"/>
<rectangle x1="0.245" y1="2.735" x2="1.655" y2="2.745" layer="21"/>
<rectangle x1="3.995" y1="2.735" x2="5.395" y2="2.745" layer="21"/>
<rectangle x1="8.115" y1="2.735" x2="11.725" y2="2.745" layer="21"/>
<rectangle x1="0.245" y1="2.745" x2="1.655" y2="2.755" layer="21"/>
<rectangle x1="3.995" y1="2.745" x2="5.395" y2="2.755" layer="21"/>
<rectangle x1="8.085" y1="2.745" x2="11.715" y2="2.755" layer="21"/>
<rectangle x1="0.245" y1="2.755" x2="1.655" y2="2.765" layer="21"/>
<rectangle x1="3.995" y1="2.755" x2="5.395" y2="2.765" layer="21"/>
<rectangle x1="8.065" y1="2.755" x2="11.695" y2="2.765" layer="21"/>
<rectangle x1="0.245" y1="2.765" x2="1.655" y2="2.775" layer="21"/>
<rectangle x1="3.995" y1="2.765" x2="5.395" y2="2.775" layer="21"/>
<rectangle x1="8.045" y1="2.765" x2="11.685" y2="2.775" layer="21"/>
<rectangle x1="0.245" y1="2.775" x2="1.655" y2="2.785" layer="21"/>
<rectangle x1="3.995" y1="2.775" x2="5.395" y2="2.785" layer="21"/>
<rectangle x1="8.025" y1="2.775" x2="11.665" y2="2.785" layer="21"/>
<rectangle x1="0.245" y1="2.785" x2="1.655" y2="2.795" layer="21"/>
<rectangle x1="3.995" y1="2.785" x2="5.395" y2="2.795" layer="21"/>
<rectangle x1="8.005" y1="2.785" x2="11.655" y2="2.795" layer="21"/>
<rectangle x1="0.245" y1="2.795" x2="1.655" y2="2.805" layer="21"/>
<rectangle x1="3.995" y1="2.795" x2="5.395" y2="2.805" layer="21"/>
<rectangle x1="7.985" y1="2.795" x2="11.635" y2="2.805" layer="21"/>
<rectangle x1="0.245" y1="2.805" x2="1.655" y2="2.815" layer="21"/>
<rectangle x1="3.995" y1="2.805" x2="5.395" y2="2.815" layer="21"/>
<rectangle x1="7.965" y1="2.805" x2="11.625" y2="2.815" layer="21"/>
<rectangle x1="0.245" y1="2.815" x2="1.655" y2="2.825" layer="21"/>
<rectangle x1="3.995" y1="2.815" x2="5.395" y2="2.825" layer="21"/>
<rectangle x1="7.945" y1="2.815" x2="11.605" y2="2.825" layer="21"/>
<rectangle x1="0.245" y1="2.825" x2="1.655" y2="2.835" layer="21"/>
<rectangle x1="3.995" y1="2.825" x2="5.395" y2="2.835" layer="21"/>
<rectangle x1="7.925" y1="2.825" x2="11.585" y2="2.835" layer="21"/>
<rectangle x1="0.245" y1="2.835" x2="1.655" y2="2.845" layer="21"/>
<rectangle x1="3.995" y1="2.835" x2="5.395" y2="2.845" layer="21"/>
<rectangle x1="7.905" y1="2.835" x2="11.575" y2="2.845" layer="21"/>
<rectangle x1="0.245" y1="2.845" x2="1.655" y2="2.855" layer="21"/>
<rectangle x1="3.995" y1="2.845" x2="5.395" y2="2.855" layer="21"/>
<rectangle x1="7.895" y1="2.845" x2="11.555" y2="2.855" layer="21"/>
<rectangle x1="0.245" y1="2.855" x2="1.655" y2="2.865" layer="21"/>
<rectangle x1="3.995" y1="2.855" x2="5.395" y2="2.865" layer="21"/>
<rectangle x1="7.875" y1="2.855" x2="11.535" y2="2.865" layer="21"/>
<rectangle x1="0.245" y1="2.865" x2="1.655" y2="2.875" layer="21"/>
<rectangle x1="3.995" y1="2.865" x2="5.395" y2="2.875" layer="21"/>
<rectangle x1="7.855" y1="2.865" x2="11.515" y2="2.875" layer="21"/>
<rectangle x1="0.245" y1="2.875" x2="1.655" y2="2.885" layer="21"/>
<rectangle x1="3.995" y1="2.875" x2="5.395" y2="2.885" layer="21"/>
<rectangle x1="7.845" y1="2.875" x2="11.495" y2="2.885" layer="21"/>
<rectangle x1="0.245" y1="2.885" x2="1.655" y2="2.895" layer="21"/>
<rectangle x1="3.995" y1="2.885" x2="5.395" y2="2.895" layer="21"/>
<rectangle x1="7.825" y1="2.885" x2="11.475" y2="2.895" layer="21"/>
<rectangle x1="0.245" y1="2.895" x2="1.655" y2="2.905" layer="21"/>
<rectangle x1="3.995" y1="2.895" x2="5.395" y2="2.905" layer="21"/>
<rectangle x1="7.815" y1="2.895" x2="11.455" y2="2.905" layer="21"/>
<rectangle x1="0.245" y1="2.905" x2="1.655" y2="2.915" layer="21"/>
<rectangle x1="3.995" y1="2.905" x2="5.395" y2="2.915" layer="21"/>
<rectangle x1="7.795" y1="2.905" x2="11.435" y2="2.915" layer="21"/>
<rectangle x1="0.245" y1="2.915" x2="1.655" y2="2.925" layer="21"/>
<rectangle x1="3.995" y1="2.915" x2="5.405" y2="2.925" layer="21"/>
<rectangle x1="7.785" y1="2.915" x2="11.415" y2="2.925" layer="21"/>
<rectangle x1="0.245" y1="2.925" x2="1.655" y2="2.935" layer="21"/>
<rectangle x1="3.995" y1="2.925" x2="5.405" y2="2.935" layer="21"/>
<rectangle x1="7.765" y1="2.925" x2="11.395" y2="2.935" layer="21"/>
<rectangle x1="0.245" y1="2.935" x2="1.655" y2="2.945" layer="21"/>
<rectangle x1="3.995" y1="2.935" x2="5.405" y2="2.945" layer="21"/>
<rectangle x1="7.755" y1="2.935" x2="11.375" y2="2.945" layer="21"/>
<rectangle x1="0.245" y1="2.945" x2="1.655" y2="2.955" layer="21"/>
<rectangle x1="3.995" y1="2.945" x2="5.405" y2="2.955" layer="21"/>
<rectangle x1="7.745" y1="2.945" x2="11.355" y2="2.955" layer="21"/>
<rectangle x1="0.245" y1="2.955" x2="1.655" y2="2.965" layer="21"/>
<rectangle x1="3.995" y1="2.955" x2="5.405" y2="2.965" layer="21"/>
<rectangle x1="7.725" y1="2.955" x2="11.325" y2="2.965" layer="21"/>
<rectangle x1="0.245" y1="2.965" x2="1.655" y2="2.975" layer="21"/>
<rectangle x1="3.995" y1="2.965" x2="5.405" y2="2.975" layer="21"/>
<rectangle x1="7.715" y1="2.965" x2="11.305" y2="2.975" layer="21"/>
<rectangle x1="0.245" y1="2.975" x2="1.655" y2="2.985" layer="21"/>
<rectangle x1="3.995" y1="2.975" x2="5.405" y2="2.985" layer="21"/>
<rectangle x1="7.705" y1="2.975" x2="11.285" y2="2.985" layer="21"/>
<rectangle x1="0.245" y1="2.985" x2="1.655" y2="2.995" layer="21"/>
<rectangle x1="3.995" y1="2.985" x2="5.405" y2="2.995" layer="21"/>
<rectangle x1="7.695" y1="2.985" x2="11.255" y2="2.995" layer="21"/>
<rectangle x1="0.245" y1="2.995" x2="1.655" y2="3.005" layer="21"/>
<rectangle x1="3.995" y1="2.995" x2="5.405" y2="3.005" layer="21"/>
<rectangle x1="7.675" y1="2.995" x2="11.235" y2="3.005" layer="21"/>
<rectangle x1="0.245" y1="3.005" x2="1.655" y2="3.015" layer="21"/>
<rectangle x1="3.995" y1="3.005" x2="5.405" y2="3.015" layer="21"/>
<rectangle x1="7.665" y1="3.005" x2="11.205" y2="3.015" layer="21"/>
<rectangle x1="0.245" y1="3.015" x2="1.655" y2="3.025" layer="21"/>
<rectangle x1="3.995" y1="3.015" x2="5.405" y2="3.025" layer="21"/>
<rectangle x1="7.655" y1="3.015" x2="11.185" y2="3.025" layer="21"/>
<rectangle x1="0.245" y1="3.025" x2="1.655" y2="3.035" layer="21"/>
<rectangle x1="3.995" y1="3.025" x2="5.405" y2="3.035" layer="21"/>
<rectangle x1="7.645" y1="3.025" x2="11.155" y2="3.035" layer="21"/>
<rectangle x1="0.245" y1="3.035" x2="1.655" y2="3.045" layer="21"/>
<rectangle x1="3.995" y1="3.035" x2="5.405" y2="3.045" layer="21"/>
<rectangle x1="7.635" y1="3.035" x2="11.125" y2="3.045" layer="21"/>
<rectangle x1="0.245" y1="3.045" x2="1.655" y2="3.055" layer="21"/>
<rectangle x1="3.995" y1="3.045" x2="5.405" y2="3.055" layer="21"/>
<rectangle x1="7.625" y1="3.045" x2="11.095" y2="3.055" layer="21"/>
<rectangle x1="0.245" y1="3.055" x2="1.655" y2="3.065" layer="21"/>
<rectangle x1="3.995" y1="3.055" x2="5.405" y2="3.065" layer="21"/>
<rectangle x1="7.615" y1="3.055" x2="11.075" y2="3.065" layer="21"/>
<rectangle x1="0.245" y1="3.065" x2="1.655" y2="3.075" layer="21"/>
<rectangle x1="3.995" y1="3.065" x2="5.405" y2="3.075" layer="21"/>
<rectangle x1="7.605" y1="3.065" x2="11.045" y2="3.075" layer="21"/>
<rectangle x1="0.245" y1="3.075" x2="1.655" y2="3.085" layer="21"/>
<rectangle x1="3.995" y1="3.075" x2="5.405" y2="3.085" layer="21"/>
<rectangle x1="7.595" y1="3.075" x2="11.015" y2="3.085" layer="21"/>
<rectangle x1="0.245" y1="3.085" x2="1.655" y2="3.095" layer="21"/>
<rectangle x1="3.995" y1="3.085" x2="5.405" y2="3.095" layer="21"/>
<rectangle x1="7.585" y1="3.085" x2="10.985" y2="3.095" layer="21"/>
<rectangle x1="0.245" y1="3.095" x2="1.655" y2="3.105" layer="21"/>
<rectangle x1="3.995" y1="3.095" x2="5.405" y2="3.105" layer="21"/>
<rectangle x1="7.575" y1="3.095" x2="10.955" y2="3.105" layer="21"/>
<rectangle x1="0.245" y1="3.105" x2="1.655" y2="3.115" layer="21"/>
<rectangle x1="3.995" y1="3.105" x2="5.405" y2="3.115" layer="21"/>
<rectangle x1="7.565" y1="3.105" x2="10.925" y2="3.115" layer="21"/>
<rectangle x1="0.245" y1="3.115" x2="1.655" y2="3.125" layer="21"/>
<rectangle x1="3.995" y1="3.115" x2="5.405" y2="3.125" layer="21"/>
<rectangle x1="7.555" y1="3.115" x2="10.895" y2="3.125" layer="21"/>
<rectangle x1="0.245" y1="3.125" x2="1.655" y2="3.135" layer="21"/>
<rectangle x1="3.995" y1="3.125" x2="5.405" y2="3.135" layer="21"/>
<rectangle x1="7.555" y1="3.125" x2="10.855" y2="3.135" layer="21"/>
<rectangle x1="0.245" y1="3.135" x2="1.655" y2="3.145" layer="21"/>
<rectangle x1="3.995" y1="3.135" x2="5.405" y2="3.145" layer="21"/>
<rectangle x1="7.545" y1="3.135" x2="10.825" y2="3.145" layer="21"/>
<rectangle x1="0.245" y1="3.145" x2="1.655" y2="3.155" layer="21"/>
<rectangle x1="3.995" y1="3.145" x2="5.405" y2="3.155" layer="21"/>
<rectangle x1="7.535" y1="3.145" x2="10.795" y2="3.155" layer="21"/>
<rectangle x1="0.245" y1="3.155" x2="1.655" y2="3.165" layer="21"/>
<rectangle x1="3.995" y1="3.155" x2="5.405" y2="3.165" layer="21"/>
<rectangle x1="7.525" y1="3.155" x2="10.755" y2="3.165" layer="21"/>
<rectangle x1="0.245" y1="3.165" x2="1.655" y2="3.175" layer="21"/>
<rectangle x1="3.995" y1="3.165" x2="5.405" y2="3.175" layer="21"/>
<rectangle x1="7.515" y1="3.165" x2="10.725" y2="3.175" layer="21"/>
<rectangle x1="0.245" y1="3.175" x2="1.655" y2="3.185" layer="21"/>
<rectangle x1="3.995" y1="3.175" x2="5.405" y2="3.185" layer="21"/>
<rectangle x1="7.515" y1="3.175" x2="10.695" y2="3.185" layer="21"/>
<rectangle x1="0.245" y1="3.185" x2="1.655" y2="3.195" layer="21"/>
<rectangle x1="3.995" y1="3.185" x2="5.405" y2="3.195" layer="21"/>
<rectangle x1="7.505" y1="3.185" x2="10.655" y2="3.195" layer="21"/>
<rectangle x1="0.245" y1="3.195" x2="1.655" y2="3.205" layer="21"/>
<rectangle x1="3.995" y1="3.195" x2="5.405" y2="3.205" layer="21"/>
<rectangle x1="7.495" y1="3.195" x2="10.625" y2="3.205" layer="21"/>
<rectangle x1="0.245" y1="3.205" x2="1.655" y2="3.215" layer="21"/>
<rectangle x1="3.995" y1="3.205" x2="5.405" y2="3.215" layer="21"/>
<rectangle x1="7.495" y1="3.205" x2="10.585" y2="3.215" layer="21"/>
<rectangle x1="0.245" y1="3.215" x2="1.655" y2="3.225" layer="21"/>
<rectangle x1="3.995" y1="3.215" x2="5.405" y2="3.225" layer="21"/>
<rectangle x1="7.485" y1="3.215" x2="10.545" y2="3.225" layer="21"/>
<rectangle x1="0.245" y1="3.225" x2="1.655" y2="3.235" layer="21"/>
<rectangle x1="3.995" y1="3.225" x2="5.405" y2="3.235" layer="21"/>
<rectangle x1="7.475" y1="3.225" x2="10.515" y2="3.235" layer="21"/>
<rectangle x1="0.245" y1="3.235" x2="1.655" y2="3.245" layer="21"/>
<rectangle x1="3.995" y1="3.235" x2="5.405" y2="3.245" layer="21"/>
<rectangle x1="7.475" y1="3.235" x2="10.475" y2="3.245" layer="21"/>
<rectangle x1="0.245" y1="3.245" x2="1.655" y2="3.255" layer="21"/>
<rectangle x1="3.995" y1="3.245" x2="5.405" y2="3.255" layer="21"/>
<rectangle x1="7.465" y1="3.245" x2="10.445" y2="3.255" layer="21"/>
<rectangle x1="0.245" y1="3.255" x2="1.655" y2="3.265" layer="21"/>
<rectangle x1="3.995" y1="3.255" x2="5.405" y2="3.265" layer="21"/>
<rectangle x1="7.455" y1="3.255" x2="10.405" y2="3.265" layer="21"/>
<rectangle x1="0.245" y1="3.265" x2="1.655" y2="3.275" layer="21"/>
<rectangle x1="3.995" y1="3.265" x2="5.405" y2="3.275" layer="21"/>
<rectangle x1="7.455" y1="3.265" x2="10.365" y2="3.275" layer="21"/>
<rectangle x1="0.245" y1="3.275" x2="1.655" y2="3.285" layer="21"/>
<rectangle x1="3.995" y1="3.275" x2="5.405" y2="3.285" layer="21"/>
<rectangle x1="7.445" y1="3.275" x2="10.325" y2="3.285" layer="21"/>
<rectangle x1="0.245" y1="3.285" x2="1.655" y2="3.295" layer="21"/>
<rectangle x1="3.995" y1="3.285" x2="5.405" y2="3.295" layer="21"/>
<rectangle x1="7.445" y1="3.285" x2="10.285" y2="3.295" layer="21"/>
<rectangle x1="0.245" y1="3.295" x2="1.655" y2="3.305" layer="21"/>
<rectangle x1="3.995" y1="3.295" x2="5.405" y2="3.305" layer="21"/>
<rectangle x1="7.435" y1="3.295" x2="10.245" y2="3.305" layer="21"/>
<rectangle x1="0.245" y1="3.305" x2="1.655" y2="3.315" layer="21"/>
<rectangle x1="3.995" y1="3.305" x2="5.405" y2="3.315" layer="21"/>
<rectangle x1="7.435" y1="3.305" x2="10.205" y2="3.315" layer="21"/>
<rectangle x1="0.245" y1="3.315" x2="1.655" y2="3.325" layer="21"/>
<rectangle x1="3.995" y1="3.315" x2="5.405" y2="3.325" layer="21"/>
<rectangle x1="7.425" y1="3.315" x2="10.165" y2="3.325" layer="21"/>
<rectangle x1="0.245" y1="3.325" x2="1.655" y2="3.335" layer="21"/>
<rectangle x1="3.995" y1="3.325" x2="5.405" y2="3.335" layer="21"/>
<rectangle x1="7.425" y1="3.325" x2="10.125" y2="3.335" layer="21"/>
<rectangle x1="0.245" y1="3.335" x2="1.655" y2="3.345" layer="21"/>
<rectangle x1="3.995" y1="3.335" x2="5.405" y2="3.345" layer="21"/>
<rectangle x1="7.415" y1="3.335" x2="10.085" y2="3.345" layer="21"/>
<rectangle x1="0.245" y1="3.345" x2="1.655" y2="3.355" layer="21"/>
<rectangle x1="3.995" y1="3.345" x2="5.405" y2="3.355" layer="21"/>
<rectangle x1="7.415" y1="3.345" x2="10.045" y2="3.355" layer="21"/>
<rectangle x1="0.245" y1="3.355" x2="1.655" y2="3.365" layer="21"/>
<rectangle x1="3.995" y1="3.355" x2="5.405" y2="3.365" layer="21"/>
<rectangle x1="7.405" y1="3.355" x2="10.005" y2="3.365" layer="21"/>
<rectangle x1="0.245" y1="3.365" x2="1.655" y2="3.375" layer="21"/>
<rectangle x1="3.995" y1="3.365" x2="5.405" y2="3.375" layer="21"/>
<rectangle x1="7.405" y1="3.365" x2="9.965" y2="3.375" layer="21"/>
<rectangle x1="0.245" y1="3.375" x2="1.655" y2="3.385" layer="21"/>
<rectangle x1="3.995" y1="3.375" x2="5.405" y2="3.385" layer="21"/>
<rectangle x1="7.395" y1="3.375" x2="9.925" y2="3.385" layer="21"/>
<rectangle x1="0.245" y1="3.385" x2="1.655" y2="3.395" layer="21"/>
<rectangle x1="3.995" y1="3.385" x2="5.405" y2="3.395" layer="21"/>
<rectangle x1="7.395" y1="3.385" x2="9.885" y2="3.395" layer="21"/>
<rectangle x1="0.245" y1="3.395" x2="1.655" y2="3.405" layer="21"/>
<rectangle x1="3.995" y1="3.395" x2="5.405" y2="3.405" layer="21"/>
<rectangle x1="7.385" y1="3.395" x2="9.845" y2="3.405" layer="21"/>
<rectangle x1="0.245" y1="3.405" x2="1.655" y2="3.415" layer="21"/>
<rectangle x1="3.995" y1="3.405" x2="5.405" y2="3.415" layer="21"/>
<rectangle x1="7.385" y1="3.405" x2="9.795" y2="3.415" layer="21"/>
<rectangle x1="0.245" y1="3.415" x2="1.655" y2="3.425" layer="21"/>
<rectangle x1="3.995" y1="3.415" x2="5.405" y2="3.425" layer="21"/>
<rectangle x1="7.385" y1="3.415" x2="9.755" y2="3.425" layer="21"/>
<rectangle x1="0.245" y1="3.425" x2="1.655" y2="3.435" layer="21"/>
<rectangle x1="3.995" y1="3.425" x2="5.405" y2="3.435" layer="21"/>
<rectangle x1="7.375" y1="3.425" x2="9.715" y2="3.435" layer="21"/>
<rectangle x1="0.245" y1="3.435" x2="1.655" y2="3.445" layer="21"/>
<rectangle x1="3.995" y1="3.435" x2="5.405" y2="3.445" layer="21"/>
<rectangle x1="7.375" y1="3.435" x2="9.665" y2="3.445" layer="21"/>
<rectangle x1="0.245" y1="3.445" x2="1.655" y2="3.455" layer="21"/>
<rectangle x1="4.005" y1="3.445" x2="5.405" y2="3.455" layer="21"/>
<rectangle x1="7.365" y1="3.445" x2="9.625" y2="3.455" layer="21"/>
<rectangle x1="0.245" y1="3.455" x2="1.655" y2="3.465" layer="21"/>
<rectangle x1="4.005" y1="3.455" x2="5.405" y2="3.465" layer="21"/>
<rectangle x1="7.365" y1="3.455" x2="9.585" y2="3.465" layer="21"/>
<rectangle x1="0.245" y1="3.465" x2="1.655" y2="3.475" layer="21"/>
<rectangle x1="4.005" y1="3.465" x2="5.405" y2="3.475" layer="21"/>
<rectangle x1="7.365" y1="3.465" x2="9.545" y2="3.475" layer="21"/>
<rectangle x1="0.245" y1="3.475" x2="1.655" y2="3.485" layer="21"/>
<rectangle x1="4.005" y1="3.475" x2="5.405" y2="3.485" layer="21"/>
<rectangle x1="7.355" y1="3.475" x2="9.495" y2="3.485" layer="21"/>
<rectangle x1="0.245" y1="3.485" x2="1.655" y2="3.495" layer="21"/>
<rectangle x1="4.005" y1="3.485" x2="5.405" y2="3.495" layer="21"/>
<rectangle x1="7.355" y1="3.485" x2="9.455" y2="3.495" layer="21"/>
<rectangle x1="0.245" y1="3.495" x2="1.655" y2="3.505" layer="21"/>
<rectangle x1="4.005" y1="3.495" x2="5.405" y2="3.505" layer="21"/>
<rectangle x1="7.355" y1="3.495" x2="9.415" y2="3.505" layer="21"/>
<rectangle x1="0.245" y1="3.505" x2="1.655" y2="3.515" layer="21"/>
<rectangle x1="4.005" y1="3.505" x2="5.405" y2="3.515" layer="21"/>
<rectangle x1="7.345" y1="3.505" x2="9.375" y2="3.515" layer="21"/>
<rectangle x1="0.245" y1="3.515" x2="1.655" y2="3.525" layer="21"/>
<rectangle x1="4.005" y1="3.515" x2="5.405" y2="3.525" layer="21"/>
<rectangle x1="7.345" y1="3.515" x2="9.335" y2="3.525" layer="21"/>
<rectangle x1="0.245" y1="3.525" x2="1.655" y2="3.535" layer="21"/>
<rectangle x1="4.005" y1="3.525" x2="5.405" y2="3.535" layer="21"/>
<rectangle x1="7.345" y1="3.525" x2="9.305" y2="3.535" layer="21"/>
<rectangle x1="0.245" y1="3.535" x2="1.655" y2="3.545" layer="21"/>
<rectangle x1="4.005" y1="3.535" x2="5.415" y2="3.545" layer="21"/>
<rectangle x1="7.345" y1="3.535" x2="9.265" y2="3.545" layer="21"/>
<rectangle x1="0.245" y1="3.545" x2="1.655" y2="3.555" layer="21"/>
<rectangle x1="4.005" y1="3.545" x2="5.415" y2="3.555" layer="21"/>
<rectangle x1="7.335" y1="3.545" x2="9.225" y2="3.555" layer="21"/>
<rectangle x1="0.245" y1="3.555" x2="1.655" y2="3.565" layer="21"/>
<rectangle x1="4.005" y1="3.555" x2="5.415" y2="3.565" layer="21"/>
<rectangle x1="7.335" y1="3.555" x2="9.195" y2="3.565" layer="21"/>
<rectangle x1="0.245" y1="3.565" x2="1.655" y2="3.575" layer="21"/>
<rectangle x1="4.005" y1="3.565" x2="5.415" y2="3.575" layer="21"/>
<rectangle x1="7.335" y1="3.565" x2="9.165" y2="3.575" layer="21"/>
<rectangle x1="0.245" y1="3.575" x2="1.655" y2="3.585" layer="21"/>
<rectangle x1="4.005" y1="3.575" x2="5.415" y2="3.585" layer="21"/>
<rectangle x1="7.325" y1="3.575" x2="9.125" y2="3.585" layer="21"/>
<rectangle x1="0.245" y1="3.585" x2="1.655" y2="3.595" layer="21"/>
<rectangle x1="4.005" y1="3.585" x2="5.415" y2="3.595" layer="21"/>
<rectangle x1="7.325" y1="3.585" x2="9.095" y2="3.595" layer="21"/>
<rectangle x1="0.245" y1="3.595" x2="1.655" y2="3.605" layer="21"/>
<rectangle x1="4.005" y1="3.595" x2="5.415" y2="3.605" layer="21"/>
<rectangle x1="7.325" y1="3.595" x2="9.065" y2="3.605" layer="21"/>
<rectangle x1="0.245" y1="3.605" x2="1.655" y2="3.615" layer="21"/>
<rectangle x1="4.005" y1="3.605" x2="5.415" y2="3.615" layer="21"/>
<rectangle x1="7.325" y1="3.605" x2="9.035" y2="3.615" layer="21"/>
<rectangle x1="0.245" y1="3.615" x2="1.655" y2="3.625" layer="21"/>
<rectangle x1="4.005" y1="3.615" x2="5.415" y2="3.625" layer="21"/>
<rectangle x1="7.325" y1="3.615" x2="9.015" y2="3.625" layer="21"/>
<rectangle x1="0.245" y1="3.625" x2="1.655" y2="3.635" layer="21"/>
<rectangle x1="4.005" y1="3.625" x2="5.415" y2="3.635" layer="21"/>
<rectangle x1="7.315" y1="3.625" x2="8.985" y2="3.635" layer="21"/>
<rectangle x1="0.245" y1="3.635" x2="1.655" y2="3.645" layer="21"/>
<rectangle x1="4.005" y1="3.635" x2="5.415" y2="3.645" layer="21"/>
<rectangle x1="7.315" y1="3.635" x2="8.955" y2="3.645" layer="21"/>
<rectangle x1="0.245" y1="3.645" x2="1.655" y2="3.655" layer="21"/>
<rectangle x1="4.005" y1="3.645" x2="5.415" y2="3.655" layer="21"/>
<rectangle x1="7.315" y1="3.645" x2="8.935" y2="3.655" layer="21"/>
<rectangle x1="0.245" y1="3.655" x2="1.655" y2="3.665" layer="21"/>
<rectangle x1="4.005" y1="3.655" x2="5.415" y2="3.665" layer="21"/>
<rectangle x1="7.315" y1="3.655" x2="8.905" y2="3.665" layer="21"/>
<rectangle x1="0.245" y1="3.665" x2="1.655" y2="3.675" layer="21"/>
<rectangle x1="4.005" y1="3.665" x2="5.415" y2="3.675" layer="21"/>
<rectangle x1="7.315" y1="3.665" x2="8.885" y2="3.675" layer="21"/>
<rectangle x1="0.245" y1="3.675" x2="1.655" y2="3.685" layer="21"/>
<rectangle x1="4.005" y1="3.675" x2="5.415" y2="3.685" layer="21"/>
<rectangle x1="7.305" y1="3.675" x2="8.855" y2="3.685" layer="21"/>
<rectangle x1="0.245" y1="3.685" x2="1.655" y2="3.695" layer="21"/>
<rectangle x1="4.005" y1="3.685" x2="5.415" y2="3.695" layer="21"/>
<rectangle x1="7.305" y1="3.685" x2="8.835" y2="3.695" layer="21"/>
<rectangle x1="0.245" y1="3.695" x2="1.655" y2="3.705" layer="21"/>
<rectangle x1="4.005" y1="3.695" x2="5.415" y2="3.705" layer="21"/>
<rectangle x1="7.305" y1="3.695" x2="8.815" y2="3.705" layer="21"/>
<rectangle x1="0.245" y1="3.705" x2="1.655" y2="3.715" layer="21"/>
<rectangle x1="4.005" y1="3.705" x2="5.415" y2="3.715" layer="21"/>
<rectangle x1="7.305" y1="3.705" x2="8.795" y2="3.715" layer="21"/>
<rectangle x1="0.245" y1="3.715" x2="1.655" y2="3.725" layer="21"/>
<rectangle x1="4.005" y1="3.715" x2="5.415" y2="3.725" layer="21"/>
<rectangle x1="7.305" y1="3.715" x2="8.775" y2="3.725" layer="21"/>
<rectangle x1="0.245" y1="3.725" x2="1.655" y2="3.735" layer="21"/>
<rectangle x1="4.005" y1="3.725" x2="5.415" y2="3.735" layer="21"/>
<rectangle x1="7.305" y1="3.725" x2="8.765" y2="3.735" layer="21"/>
<rectangle x1="0.245" y1="3.735" x2="1.655" y2="3.745" layer="21"/>
<rectangle x1="4.005" y1="3.735" x2="5.415" y2="3.745" layer="21"/>
<rectangle x1="7.305" y1="3.735" x2="8.745" y2="3.745" layer="21"/>
<rectangle x1="0.245" y1="3.745" x2="1.655" y2="3.755" layer="21"/>
<rectangle x1="4.005" y1="3.745" x2="5.415" y2="3.755" layer="21"/>
<rectangle x1="7.305" y1="3.745" x2="8.735" y2="3.755" layer="21"/>
<rectangle x1="0.245" y1="3.755" x2="1.655" y2="3.765" layer="21"/>
<rectangle x1="4.005" y1="3.755" x2="5.415" y2="3.765" layer="21"/>
<rectangle x1="7.305" y1="3.755" x2="8.715" y2="3.765" layer="21"/>
<rectangle x1="0.245" y1="3.765" x2="1.655" y2="3.775" layer="21"/>
<rectangle x1="4.005" y1="3.765" x2="5.415" y2="3.775" layer="21"/>
<rectangle x1="7.305" y1="3.765" x2="8.705" y2="3.775" layer="21"/>
<rectangle x1="0.245" y1="3.775" x2="1.655" y2="3.785" layer="21"/>
<rectangle x1="4.005" y1="3.775" x2="5.415" y2="3.785" layer="21"/>
<rectangle x1="7.305" y1="3.775" x2="8.695" y2="3.785" layer="21"/>
<rectangle x1="0.245" y1="3.785" x2="1.655" y2="3.795" layer="21"/>
<rectangle x1="4.005" y1="3.785" x2="5.415" y2="3.795" layer="21"/>
<rectangle x1="7.305" y1="3.785" x2="8.685" y2="3.795" layer="21"/>
<rectangle x1="0.245" y1="3.795" x2="1.655" y2="3.805" layer="21"/>
<rectangle x1="4.005" y1="3.795" x2="5.415" y2="3.805" layer="21"/>
<rectangle x1="7.305" y1="3.795" x2="8.675" y2="3.805" layer="21"/>
<rectangle x1="0.245" y1="3.805" x2="1.655" y2="3.815" layer="21"/>
<rectangle x1="4.005" y1="3.805" x2="5.415" y2="3.815" layer="21"/>
<rectangle x1="7.305" y1="3.805" x2="8.675" y2="3.815" layer="21"/>
<rectangle x1="0.245" y1="3.815" x2="1.655" y2="3.825" layer="21"/>
<rectangle x1="4.005" y1="3.815" x2="5.415" y2="3.825" layer="21"/>
<rectangle x1="7.305" y1="3.815" x2="8.665" y2="3.825" layer="21"/>
<rectangle x1="0.245" y1="3.825" x2="1.655" y2="3.835" layer="21"/>
<rectangle x1="4.005" y1="3.825" x2="5.415" y2="3.835" layer="21"/>
<rectangle x1="7.305" y1="3.825" x2="8.655" y2="3.835" layer="21"/>
<rectangle x1="0.245" y1="3.835" x2="1.655" y2="3.845" layer="21"/>
<rectangle x1="4.005" y1="3.835" x2="5.415" y2="3.845" layer="21"/>
<rectangle x1="7.305" y1="3.835" x2="8.655" y2="3.845" layer="21"/>
<rectangle x1="0.245" y1="3.845" x2="1.655" y2="3.855" layer="21"/>
<rectangle x1="4.005" y1="3.845" x2="5.415" y2="3.855" layer="21"/>
<rectangle x1="7.305" y1="3.845" x2="8.655" y2="3.855" layer="21"/>
<rectangle x1="0.245" y1="3.855" x2="1.655" y2="3.865" layer="21"/>
<rectangle x1="4.005" y1="3.855" x2="5.415" y2="3.865" layer="21"/>
<rectangle x1="7.305" y1="3.855" x2="8.645" y2="3.865" layer="21"/>
<rectangle x1="0.245" y1="3.865" x2="1.655" y2="3.875" layer="21"/>
<rectangle x1="4.005" y1="3.865" x2="5.415" y2="3.875" layer="21"/>
<rectangle x1="7.305" y1="3.865" x2="8.645" y2="3.875" layer="21"/>
<rectangle x1="0.245" y1="3.875" x2="1.655" y2="3.885" layer="21"/>
<rectangle x1="4.005" y1="3.875" x2="5.415" y2="3.885" layer="21"/>
<rectangle x1="7.305" y1="3.875" x2="8.645" y2="3.885" layer="21"/>
<rectangle x1="0.245" y1="3.885" x2="1.655" y2="3.895" layer="21"/>
<rectangle x1="4.005" y1="3.885" x2="5.415" y2="3.895" layer="21"/>
<rectangle x1="7.305" y1="3.885" x2="8.635" y2="3.895" layer="21"/>
<rectangle x1="0.245" y1="3.895" x2="1.655" y2="3.905" layer="21"/>
<rectangle x1="4.005" y1="3.895" x2="5.415" y2="3.905" layer="21"/>
<rectangle x1="7.305" y1="3.895" x2="8.635" y2="3.905" layer="21"/>
<rectangle x1="0.245" y1="3.905" x2="1.655" y2="3.915" layer="21"/>
<rectangle x1="4.005" y1="3.905" x2="5.415" y2="3.915" layer="21"/>
<rectangle x1="7.305" y1="3.905" x2="8.635" y2="3.915" layer="21"/>
<rectangle x1="0.245" y1="3.915" x2="1.655" y2="3.925" layer="21"/>
<rectangle x1="4.005" y1="3.915" x2="5.415" y2="3.925" layer="21"/>
<rectangle x1="7.305" y1="3.915" x2="8.635" y2="3.925" layer="21"/>
<rectangle x1="0.245" y1="3.925" x2="1.655" y2="3.935" layer="21"/>
<rectangle x1="4.005" y1="3.925" x2="5.415" y2="3.935" layer="21"/>
<rectangle x1="7.305" y1="3.925" x2="8.635" y2="3.935" layer="21"/>
<rectangle x1="0.245" y1="3.935" x2="1.655" y2="3.945" layer="21"/>
<rectangle x1="4.005" y1="3.935" x2="5.415" y2="3.945" layer="21"/>
<rectangle x1="7.305" y1="3.935" x2="8.635" y2="3.945" layer="21"/>
<rectangle x1="0.245" y1="3.945" x2="1.655" y2="3.955" layer="21"/>
<rectangle x1="4.005" y1="3.945" x2="5.415" y2="3.955" layer="21"/>
<rectangle x1="7.305" y1="3.945" x2="8.635" y2="3.955" layer="21"/>
<rectangle x1="0.245" y1="3.955" x2="1.655" y2="3.965" layer="21"/>
<rectangle x1="4.005" y1="3.955" x2="5.415" y2="3.965" layer="21"/>
<rectangle x1="7.305" y1="3.955" x2="8.635" y2="3.965" layer="21"/>
<rectangle x1="0.245" y1="3.965" x2="1.655" y2="3.975" layer="21"/>
<rectangle x1="4.005" y1="3.965" x2="5.415" y2="3.975" layer="21"/>
<rectangle x1="7.305" y1="3.965" x2="8.635" y2="3.975" layer="21"/>
<rectangle x1="0.245" y1="3.975" x2="1.655" y2="3.985" layer="21"/>
<rectangle x1="4.005" y1="3.975" x2="5.415" y2="3.985" layer="21"/>
<rectangle x1="7.305" y1="3.975" x2="8.635" y2="3.985" layer="21"/>
<rectangle x1="0.245" y1="3.985" x2="1.655" y2="3.995" layer="21"/>
<rectangle x1="4.005" y1="3.985" x2="5.415" y2="3.995" layer="21"/>
<rectangle x1="7.305" y1="3.985" x2="8.635" y2="3.995" layer="21"/>
<rectangle x1="0.245" y1="3.995" x2="1.655" y2="4.005" layer="21"/>
<rectangle x1="4.005" y1="3.995" x2="5.415" y2="4.005" layer="21"/>
<rectangle x1="7.305" y1="3.995" x2="8.635" y2="4.005" layer="21"/>
<rectangle x1="0.245" y1="4.005" x2="1.655" y2="4.015" layer="21"/>
<rectangle x1="4.005" y1="4.005" x2="5.415" y2="4.015" layer="21"/>
<rectangle x1="7.305" y1="4.005" x2="8.635" y2="4.015" layer="21"/>
<rectangle x1="0.245" y1="4.015" x2="1.655" y2="4.025" layer="21"/>
<rectangle x1="4.005" y1="4.015" x2="5.415" y2="4.025" layer="21"/>
<rectangle x1="7.305" y1="4.015" x2="8.635" y2="4.025" layer="21"/>
<rectangle x1="0.245" y1="4.025" x2="1.655" y2="4.035" layer="21"/>
<rectangle x1="4.005" y1="4.025" x2="5.415" y2="4.035" layer="21"/>
<rectangle x1="7.305" y1="4.025" x2="8.635" y2="4.035" layer="21"/>
<rectangle x1="0.245" y1="4.035" x2="1.655" y2="4.045" layer="21"/>
<rectangle x1="4.005" y1="4.035" x2="5.415" y2="4.045" layer="21"/>
<rectangle x1="7.315" y1="4.035" x2="8.635" y2="4.045" layer="21"/>
<rectangle x1="0.245" y1="4.045" x2="1.655" y2="4.055" layer="21"/>
<rectangle x1="4.005" y1="4.045" x2="5.415" y2="4.055" layer="21"/>
<rectangle x1="7.315" y1="4.045" x2="8.645" y2="4.055" layer="21"/>
<rectangle x1="0.245" y1="4.055" x2="1.655" y2="4.065" layer="21"/>
<rectangle x1="4.005" y1="4.055" x2="5.415" y2="4.065" layer="21"/>
<rectangle x1="7.315" y1="4.055" x2="8.645" y2="4.065" layer="21"/>
<rectangle x1="0.245" y1="4.065" x2="1.655" y2="4.075" layer="21"/>
<rectangle x1="4.005" y1="4.065" x2="5.415" y2="4.075" layer="21"/>
<rectangle x1="7.315" y1="4.065" x2="8.645" y2="4.075" layer="21"/>
<rectangle x1="0.245" y1="4.075" x2="1.655" y2="4.085" layer="21"/>
<rectangle x1="4.005" y1="4.075" x2="5.415" y2="4.085" layer="21"/>
<rectangle x1="7.315" y1="4.075" x2="8.645" y2="4.085" layer="21"/>
<rectangle x1="0.245" y1="4.085" x2="1.655" y2="4.095" layer="21"/>
<rectangle x1="4.005" y1="4.085" x2="5.415" y2="4.095" layer="21"/>
<rectangle x1="7.315" y1="4.085" x2="8.655" y2="4.095" layer="21"/>
<rectangle x1="0.245" y1="4.095" x2="1.655" y2="4.105" layer="21"/>
<rectangle x1="4.005" y1="4.095" x2="5.415" y2="4.105" layer="21"/>
<rectangle x1="7.315" y1="4.095" x2="8.655" y2="4.105" layer="21"/>
<rectangle x1="0.245" y1="4.105" x2="1.655" y2="4.115" layer="21"/>
<rectangle x1="4.005" y1="4.105" x2="5.415" y2="4.115" layer="21"/>
<rectangle x1="7.315" y1="4.105" x2="8.655" y2="4.115" layer="21"/>
<rectangle x1="0.245" y1="4.115" x2="1.655" y2="4.125" layer="21"/>
<rectangle x1="4.005" y1="4.115" x2="5.415" y2="4.125" layer="21"/>
<rectangle x1="7.325" y1="4.115" x2="8.665" y2="4.125" layer="21"/>
<rectangle x1="0.245" y1="4.125" x2="1.655" y2="4.135" layer="21"/>
<rectangle x1="4.005" y1="4.125" x2="5.415" y2="4.135" layer="21"/>
<rectangle x1="7.325" y1="4.125" x2="8.665" y2="4.135" layer="21"/>
<rectangle x1="0.245" y1="4.135" x2="1.655" y2="4.145" layer="21"/>
<rectangle x1="4.005" y1="4.135" x2="5.415" y2="4.145" layer="21"/>
<rectangle x1="7.325" y1="4.135" x2="8.675" y2="4.145" layer="21"/>
<rectangle x1="0.245" y1="4.145" x2="1.655" y2="4.155" layer="21"/>
<rectangle x1="4.005" y1="4.145" x2="5.425" y2="4.155" layer="21"/>
<rectangle x1="7.325" y1="4.145" x2="8.675" y2="4.155" layer="21"/>
<rectangle x1="0.245" y1="4.155" x2="1.655" y2="4.165" layer="21"/>
<rectangle x1="4.005" y1="4.155" x2="5.425" y2="4.165" layer="21"/>
<rectangle x1="7.325" y1="4.155" x2="8.685" y2="4.165" layer="21"/>
<rectangle x1="0.245" y1="4.165" x2="1.655" y2="4.175" layer="21"/>
<rectangle x1="4.005" y1="4.165" x2="5.425" y2="4.175" layer="21"/>
<rectangle x1="7.325" y1="4.165" x2="8.685" y2="4.175" layer="21"/>
<rectangle x1="0.245" y1="4.175" x2="1.655" y2="4.185" layer="21"/>
<rectangle x1="4.005" y1="4.175" x2="5.425" y2="4.185" layer="21"/>
<rectangle x1="7.335" y1="4.175" x2="8.695" y2="4.185" layer="21"/>
<rectangle x1="0.245" y1="4.185" x2="1.655" y2="4.195" layer="21"/>
<rectangle x1="4.005" y1="4.185" x2="5.425" y2="4.195" layer="21"/>
<rectangle x1="7.335" y1="4.185" x2="8.695" y2="4.195" layer="21"/>
<rectangle x1="0.245" y1="4.195" x2="1.655" y2="4.205" layer="21"/>
<rectangle x1="4.005" y1="4.195" x2="5.425" y2="4.205" layer="21"/>
<rectangle x1="7.335" y1="4.195" x2="8.705" y2="4.205" layer="21"/>
<rectangle x1="0.245" y1="4.205" x2="1.655" y2="4.215" layer="21"/>
<rectangle x1="4.005" y1="4.205" x2="5.425" y2="4.215" layer="21"/>
<rectangle x1="7.335" y1="4.205" x2="8.715" y2="4.215" layer="21"/>
<rectangle x1="0.245" y1="4.215" x2="1.655" y2="4.225" layer="21"/>
<rectangle x1="4.005" y1="4.215" x2="5.425" y2="4.225" layer="21"/>
<rectangle x1="7.335" y1="4.215" x2="8.725" y2="4.225" layer="21"/>
<rectangle x1="0.245" y1="4.225" x2="1.655" y2="4.235" layer="21"/>
<rectangle x1="4.005" y1="4.225" x2="5.425" y2="4.235" layer="21"/>
<rectangle x1="7.345" y1="4.225" x2="8.735" y2="4.235" layer="21"/>
<rectangle x1="0.245" y1="4.235" x2="1.655" y2="4.245" layer="21"/>
<rectangle x1="4.005" y1="4.235" x2="5.425" y2="4.245" layer="21"/>
<rectangle x1="7.345" y1="4.235" x2="8.745" y2="4.245" layer="21"/>
<rectangle x1="0.245" y1="4.245" x2="1.655" y2="4.255" layer="21"/>
<rectangle x1="4.005" y1="4.245" x2="5.425" y2="4.255" layer="21"/>
<rectangle x1="7.345" y1="4.245" x2="8.755" y2="4.255" layer="21"/>
<rectangle x1="0.245" y1="4.255" x2="1.655" y2="4.265" layer="21"/>
<rectangle x1="4.005" y1="4.255" x2="5.425" y2="4.265" layer="21"/>
<rectangle x1="7.345" y1="4.255" x2="8.765" y2="4.265" layer="21"/>
<rectangle x1="0.245" y1="4.265" x2="1.655" y2="4.275" layer="21"/>
<rectangle x1="4.005" y1="4.265" x2="5.425" y2="4.275" layer="21"/>
<rectangle x1="7.355" y1="4.265" x2="8.775" y2="4.275" layer="21"/>
<rectangle x1="0.245" y1="4.275" x2="1.655" y2="4.285" layer="21"/>
<rectangle x1="4.005" y1="4.275" x2="5.425" y2="4.285" layer="21"/>
<rectangle x1="7.355" y1="4.275" x2="8.785" y2="4.285" layer="21"/>
<rectangle x1="0.245" y1="4.285" x2="1.655" y2="4.295" layer="21"/>
<rectangle x1="4.005" y1="4.285" x2="5.425" y2="4.295" layer="21"/>
<rectangle x1="7.355" y1="4.285" x2="8.805" y2="4.295" layer="21"/>
<rectangle x1="0.245" y1="4.295" x2="1.655" y2="4.305" layer="21"/>
<rectangle x1="4.005" y1="4.295" x2="5.425" y2="4.305" layer="21"/>
<rectangle x1="7.365" y1="4.295" x2="8.815" y2="4.305" layer="21"/>
<rectangle x1="0.245" y1="4.305" x2="1.655" y2="4.315" layer="21"/>
<rectangle x1="4.005" y1="4.305" x2="5.425" y2="4.315" layer="21"/>
<rectangle x1="7.365" y1="4.305" x2="8.825" y2="4.315" layer="21"/>
<rectangle x1="0.245" y1="4.315" x2="1.655" y2="4.325" layer="21"/>
<rectangle x1="4.005" y1="4.315" x2="5.425" y2="4.325" layer="21"/>
<rectangle x1="7.365" y1="4.315" x2="8.845" y2="4.325" layer="21"/>
<rectangle x1="0.245" y1="4.325" x2="1.655" y2="4.335" layer="21"/>
<rectangle x1="4.005" y1="4.325" x2="5.425" y2="4.335" layer="21"/>
<rectangle x1="7.375" y1="4.325" x2="8.865" y2="4.335" layer="21"/>
<rectangle x1="0.245" y1="4.335" x2="1.655" y2="4.345" layer="21"/>
<rectangle x1="4.005" y1="4.335" x2="5.425" y2="4.345" layer="21"/>
<rectangle x1="7.375" y1="4.335" x2="8.885" y2="4.345" layer="21"/>
<rectangle x1="0.245" y1="4.345" x2="1.655" y2="4.355" layer="21"/>
<rectangle x1="4.005" y1="4.345" x2="5.425" y2="4.355" layer="21"/>
<rectangle x1="7.375" y1="4.345" x2="8.905" y2="4.355" layer="21"/>
<rectangle x1="0.245" y1="4.355" x2="1.655" y2="4.365" layer="21"/>
<rectangle x1="4.005" y1="4.355" x2="5.425" y2="4.365" layer="21"/>
<rectangle x1="7.385" y1="4.355" x2="8.935" y2="4.365" layer="21"/>
<rectangle x1="0.245" y1="4.365" x2="1.655" y2="4.375" layer="21"/>
<rectangle x1="4.005" y1="4.365" x2="5.425" y2="4.375" layer="21"/>
<rectangle x1="7.385" y1="4.365" x2="8.965" y2="4.375" layer="21"/>
<rectangle x1="0.245" y1="4.375" x2="1.655" y2="4.385" layer="21"/>
<rectangle x1="4.005" y1="4.375" x2="5.425" y2="4.385" layer="21"/>
<rectangle x1="7.385" y1="4.375" x2="9.005" y2="4.385" layer="21"/>
<rectangle x1="0.245" y1="4.385" x2="1.655" y2="4.395" layer="21"/>
<rectangle x1="4.005" y1="4.385" x2="5.425" y2="4.395" layer="21"/>
<rectangle x1="7.375" y1="4.385" x2="9.055" y2="4.395" layer="21"/>
<rectangle x1="0.245" y1="4.395" x2="1.655" y2="4.405" layer="21"/>
<rectangle x1="4.005" y1="4.395" x2="5.425" y2="4.405" layer="21"/>
<rectangle x1="7.355" y1="4.395" x2="9.135" y2="4.405" layer="21"/>
<rectangle x1="0.245" y1="4.405" x2="1.655" y2="4.415" layer="21"/>
<rectangle x1="4.005" y1="4.405" x2="5.425" y2="4.415" layer="21"/>
<rectangle x1="7.285" y1="4.405" x2="9.265" y2="4.415" layer="21"/>
<rectangle x1="4.005" y1="4.415" x2="5.425" y2="4.425" layer="21"/>
<rectangle x1="6.975" y1="4.415" x2="9.635" y2="4.425" layer="21"/>
<rectangle x1="1.655" y1="4.425" x2="11.105" y2="4.435" layer="21"/>
<rectangle x1="1.655" y1="4.435" x2="11.115" y2="4.445" layer="21"/>
<rectangle x1="1.655" y1="4.445" x2="11.125" y2="4.455" layer="21"/>
<rectangle x1="0.245" y1="4.455" x2="0.265" y2="4.465" layer="21"/>
<rectangle x1="1.655" y1="4.455" x2="11.145" y2="4.465" layer="21"/>
<rectangle x1="0.245" y1="4.465" x2="0.275" y2="4.475" layer="21"/>
<rectangle x1="1.655" y1="4.465" x2="11.155" y2="4.475" layer="21"/>
<rectangle x1="0.245" y1="4.475" x2="0.285" y2="4.485" layer="21"/>
<rectangle x1="1.655" y1="4.475" x2="11.165" y2="4.485" layer="21"/>
<rectangle x1="0.245" y1="4.485" x2="0.295" y2="4.495" layer="21"/>
<rectangle x1="1.655" y1="4.485" x2="11.175" y2="4.495" layer="21"/>
<rectangle x1="0.245" y1="4.495" x2="0.315" y2="4.505" layer="21"/>
<rectangle x1="1.655" y1="4.495" x2="11.185" y2="4.505" layer="21"/>
<rectangle x1="0.245" y1="4.505" x2="0.325" y2="4.515" layer="21"/>
<rectangle x1="1.655" y1="4.505" x2="11.195" y2="4.515" layer="21"/>
<rectangle x1="0.245" y1="4.515" x2="0.335" y2="4.525" layer="21"/>
<rectangle x1="1.655" y1="4.515" x2="11.205" y2="4.525" layer="21"/>
<rectangle x1="0.245" y1="4.525" x2="0.345" y2="4.535" layer="21"/>
<rectangle x1="1.655" y1="4.525" x2="11.215" y2="4.535" layer="21"/>
<rectangle x1="0.245" y1="4.535" x2="0.365" y2="4.545" layer="21"/>
<rectangle x1="1.655" y1="4.535" x2="11.225" y2="4.545" layer="21"/>
<rectangle x1="0.245" y1="4.545" x2="0.375" y2="4.555" layer="21"/>
<rectangle x1="1.655" y1="4.545" x2="11.235" y2="4.555" layer="21"/>
<rectangle x1="0.245" y1="4.555" x2="0.385" y2="4.565" layer="21"/>
<rectangle x1="1.655" y1="4.555" x2="11.245" y2="4.565" layer="21"/>
<rectangle x1="0.245" y1="4.565" x2="0.395" y2="4.575" layer="21"/>
<rectangle x1="1.655" y1="4.565" x2="11.255" y2="4.575" layer="21"/>
<rectangle x1="0.245" y1="4.575" x2="0.415" y2="4.585" layer="21"/>
<rectangle x1="1.655" y1="4.575" x2="11.265" y2="4.585" layer="21"/>
<rectangle x1="0.245" y1="4.585" x2="0.425" y2="4.595" layer="21"/>
<rectangle x1="1.655" y1="4.585" x2="11.275" y2="4.595" layer="21"/>
<rectangle x1="0.245" y1="4.595" x2="0.435" y2="4.605" layer="21"/>
<rectangle x1="1.655" y1="4.595" x2="11.295" y2="4.605" layer="21"/>
<rectangle x1="0.245" y1="4.605" x2="0.445" y2="4.615" layer="21"/>
<rectangle x1="1.655" y1="4.605" x2="11.305" y2="4.615" layer="21"/>
<rectangle x1="0.245" y1="4.615" x2="0.465" y2="4.625" layer="21"/>
<rectangle x1="1.655" y1="4.615" x2="11.315" y2="4.625" layer="21"/>
<rectangle x1="0.245" y1="4.625" x2="0.475" y2="4.635" layer="21"/>
<rectangle x1="1.655" y1="4.625" x2="11.325" y2="4.635" layer="21"/>
<rectangle x1="0.245" y1="4.635" x2="0.485" y2="4.645" layer="21"/>
<rectangle x1="1.655" y1="4.635" x2="11.335" y2="4.645" layer="21"/>
<rectangle x1="0.245" y1="4.645" x2="0.505" y2="4.655" layer="21"/>
<rectangle x1="1.655" y1="4.645" x2="11.345" y2="4.655" layer="21"/>
<rectangle x1="0.245" y1="4.655" x2="0.515" y2="4.665" layer="21"/>
<rectangle x1="1.655" y1="4.655" x2="11.355" y2="4.665" layer="21"/>
<rectangle x1="0.235" y1="4.665" x2="0.525" y2="4.675" layer="21"/>
<rectangle x1="1.655" y1="4.665" x2="11.365" y2="4.675" layer="21"/>
<rectangle x1="0.235" y1="4.675" x2="0.535" y2="4.685" layer="21"/>
<rectangle x1="1.655" y1="4.675" x2="11.375" y2="4.685" layer="21"/>
<rectangle x1="0.235" y1="4.685" x2="0.555" y2="4.695" layer="21"/>
<rectangle x1="1.655" y1="4.685" x2="11.385" y2="4.695" layer="21"/>
<rectangle x1="0.235" y1="4.695" x2="0.565" y2="4.705" layer="21"/>
<rectangle x1="1.655" y1="4.695" x2="11.395" y2="4.705" layer="21"/>
<rectangle x1="0.235" y1="4.705" x2="0.575" y2="4.715" layer="21"/>
<rectangle x1="1.655" y1="4.705" x2="11.405" y2="4.715" layer="21"/>
<rectangle x1="0.235" y1="4.715" x2="0.595" y2="4.725" layer="21"/>
<rectangle x1="1.655" y1="4.715" x2="11.415" y2="4.725" layer="21"/>
<rectangle x1="0.235" y1="4.725" x2="0.605" y2="4.735" layer="21"/>
<rectangle x1="1.655" y1="4.725" x2="11.425" y2="4.735" layer="21"/>
<rectangle x1="0.235" y1="4.735" x2="0.615" y2="4.745" layer="21"/>
<rectangle x1="1.655" y1="4.735" x2="11.445" y2="4.745" layer="21"/>
<rectangle x1="0.235" y1="4.745" x2="0.625" y2="4.755" layer="21"/>
<rectangle x1="1.655" y1="4.745" x2="11.455" y2="4.755" layer="21"/>
<rectangle x1="0.235" y1="4.755" x2="0.645" y2="4.765" layer="21"/>
<rectangle x1="1.655" y1="4.755" x2="11.465" y2="4.765" layer="21"/>
<rectangle x1="0.235" y1="4.765" x2="0.655" y2="4.775" layer="21"/>
<rectangle x1="1.655" y1="4.765" x2="11.475" y2="4.775" layer="21"/>
<rectangle x1="0.235" y1="4.775" x2="0.665" y2="4.785" layer="21"/>
<rectangle x1="1.655" y1="4.775" x2="11.485" y2="4.785" layer="21"/>
<rectangle x1="0.235" y1="4.785" x2="0.685" y2="4.795" layer="21"/>
<rectangle x1="1.655" y1="4.785" x2="11.495" y2="4.795" layer="21"/>
<rectangle x1="0.235" y1="4.795" x2="0.695" y2="4.805" layer="21"/>
<rectangle x1="1.655" y1="4.795" x2="11.505" y2="4.805" layer="21"/>
<rectangle x1="0.235" y1="4.805" x2="0.705" y2="4.815" layer="21"/>
<rectangle x1="1.655" y1="4.805" x2="11.515" y2="4.815" layer="21"/>
<rectangle x1="0.235" y1="4.815" x2="0.725" y2="4.825" layer="21"/>
<rectangle x1="1.655" y1="4.815" x2="11.525" y2="4.825" layer="21"/>
<rectangle x1="0.235" y1="4.825" x2="0.735" y2="4.835" layer="21"/>
<rectangle x1="1.655" y1="4.825" x2="11.535" y2="4.835" layer="21"/>
<rectangle x1="0.235" y1="4.835" x2="0.745" y2="4.845" layer="21"/>
<rectangle x1="1.655" y1="4.835" x2="11.545" y2="4.845" layer="21"/>
<rectangle x1="0.235" y1="4.845" x2="0.755" y2="4.855" layer="21"/>
<rectangle x1="1.655" y1="4.845" x2="11.555" y2="4.855" layer="21"/>
<rectangle x1="0.235" y1="4.855" x2="0.775" y2="4.865" layer="21"/>
<rectangle x1="1.655" y1="4.855" x2="11.565" y2="4.865" layer="21"/>
<rectangle x1="0.235" y1="4.865" x2="0.785" y2="4.875" layer="21"/>
<rectangle x1="1.655" y1="4.865" x2="11.575" y2="4.875" layer="21"/>
<rectangle x1="0.235" y1="4.875" x2="0.795" y2="4.885" layer="21"/>
<rectangle x1="1.655" y1="4.875" x2="11.595" y2="4.885" layer="21"/>
<rectangle x1="0.235" y1="4.885" x2="0.815" y2="4.895" layer="21"/>
<rectangle x1="1.655" y1="4.885" x2="11.605" y2="4.895" layer="21"/>
<rectangle x1="0.235" y1="4.895" x2="0.825" y2="4.905" layer="21"/>
<rectangle x1="1.655" y1="4.895" x2="11.615" y2="4.905" layer="21"/>
<rectangle x1="0.235" y1="4.905" x2="0.835" y2="4.915" layer="21"/>
<rectangle x1="1.655" y1="4.905" x2="11.625" y2="4.915" layer="21"/>
<rectangle x1="0.235" y1="4.915" x2="0.855" y2="4.925" layer="21"/>
<rectangle x1="1.655" y1="4.915" x2="11.635" y2="4.925" layer="21"/>
<rectangle x1="0.235" y1="4.925" x2="0.865" y2="4.935" layer="21"/>
<rectangle x1="1.655" y1="4.925" x2="11.645" y2="4.935" layer="21"/>
<rectangle x1="0.235" y1="4.935" x2="0.875" y2="4.945" layer="21"/>
<rectangle x1="1.655" y1="4.935" x2="11.655" y2="4.945" layer="21"/>
<rectangle x1="0.235" y1="4.945" x2="0.885" y2="4.955" layer="21"/>
<rectangle x1="1.655" y1="4.945" x2="11.665" y2="4.955" layer="21"/>
<rectangle x1="0.225" y1="4.955" x2="0.905" y2="4.965" layer="21"/>
<rectangle x1="1.655" y1="4.955" x2="11.675" y2="4.965" layer="21"/>
<rectangle x1="0.225" y1="4.965" x2="0.915" y2="4.975" layer="21"/>
<rectangle x1="1.655" y1="4.965" x2="11.685" y2="4.975" layer="21"/>
<rectangle x1="0.225" y1="4.975" x2="0.925" y2="4.985" layer="21"/>
<rectangle x1="1.655" y1="4.975" x2="11.695" y2="4.985" layer="21"/>
<rectangle x1="0.225" y1="4.985" x2="0.945" y2="4.995" layer="21"/>
<rectangle x1="1.655" y1="4.985" x2="11.705" y2="4.995" layer="21"/>
<rectangle x1="0.225" y1="4.995" x2="0.955" y2="5.005" layer="21"/>
<rectangle x1="1.655" y1="4.995" x2="11.715" y2="5.005" layer="21"/>
<rectangle x1="0.225" y1="5.005" x2="0.965" y2="5.015" layer="21"/>
<rectangle x1="1.655" y1="5.005" x2="11.725" y2="5.015" layer="21"/>
<rectangle x1="0.225" y1="5.015" x2="0.985" y2="5.025" layer="21"/>
<rectangle x1="1.655" y1="5.015" x2="11.735" y2="5.025" layer="21"/>
<rectangle x1="0.225" y1="5.025" x2="0.995" y2="5.035" layer="21"/>
<rectangle x1="1.655" y1="5.025" x2="11.745" y2="5.035" layer="21"/>
<rectangle x1="0.225" y1="5.035" x2="1.005" y2="5.045" layer="21"/>
<rectangle x1="1.655" y1="5.035" x2="11.755" y2="5.045" layer="21"/>
<rectangle x1="0.225" y1="5.045" x2="1.025" y2="5.055" layer="21"/>
<rectangle x1="1.655" y1="5.045" x2="11.775" y2="5.055" layer="21"/>
<rectangle x1="0.225" y1="5.055" x2="1.035" y2="5.065" layer="21"/>
<rectangle x1="1.655" y1="5.055" x2="11.785" y2="5.065" layer="21"/>
<rectangle x1="0.225" y1="5.065" x2="1.045" y2="5.075" layer="21"/>
<rectangle x1="1.655" y1="5.065" x2="11.795" y2="5.075" layer="21"/>
<rectangle x1="0.225" y1="5.075" x2="1.055" y2="5.085" layer="21"/>
<rectangle x1="1.655" y1="5.075" x2="11.805" y2="5.085" layer="21"/>
<rectangle x1="0.225" y1="5.085" x2="1.075" y2="5.095" layer="21"/>
<rectangle x1="1.655" y1="5.085" x2="11.815" y2="5.095" layer="21"/>
<rectangle x1="0.225" y1="5.095" x2="1.085" y2="5.105" layer="21"/>
<rectangle x1="1.655" y1="5.095" x2="11.825" y2="5.105" layer="21"/>
<rectangle x1="0.225" y1="5.105" x2="1.095" y2="5.115" layer="21"/>
<rectangle x1="1.655" y1="5.105" x2="11.835" y2="5.115" layer="21"/>
<rectangle x1="0.215" y1="5.115" x2="1.115" y2="5.125" layer="21"/>
<rectangle x1="1.655" y1="5.115" x2="11.845" y2="5.125" layer="21"/>
<rectangle x1="0.215" y1="5.125" x2="1.125" y2="5.135" layer="21"/>
<rectangle x1="1.655" y1="5.125" x2="11.855" y2="5.135" layer="21"/>
<rectangle x1="0.215" y1="5.135" x2="1.135" y2="5.145" layer="21"/>
<rectangle x1="1.655" y1="5.135" x2="11.865" y2="5.145" layer="21"/>
<rectangle x1="0.215" y1="5.145" x2="1.155" y2="5.155" layer="21"/>
<rectangle x1="1.655" y1="5.145" x2="11.875" y2="5.155" layer="21"/>
<rectangle x1="0.215" y1="5.155" x2="1.165" y2="5.165" layer="21"/>
<rectangle x1="1.655" y1="5.155" x2="11.885" y2="5.165" layer="21"/>
<rectangle x1="0.215" y1="5.165" x2="1.175" y2="5.175" layer="21"/>
<rectangle x1="1.655" y1="5.165" x2="11.895" y2="5.175" layer="21"/>
<rectangle x1="0.215" y1="5.175" x2="1.195" y2="5.185" layer="21"/>
<rectangle x1="1.655" y1="5.175" x2="11.905" y2="5.185" layer="21"/>
<rectangle x1="0.215" y1="5.185" x2="1.205" y2="5.195" layer="21"/>
<rectangle x1="1.655" y1="5.185" x2="11.915" y2="5.195" layer="21"/>
<rectangle x1="0.215" y1="5.195" x2="1.215" y2="5.205" layer="21"/>
<rectangle x1="1.655" y1="5.195" x2="11.925" y2="5.205" layer="21"/>
<rectangle x1="0.205" y1="5.205" x2="1.225" y2="5.215" layer="21"/>
<rectangle x1="1.655" y1="5.205" x2="11.935" y2="5.215" layer="21"/>
<rectangle x1="0.205" y1="5.215" x2="1.245" y2="5.225" layer="21"/>
<rectangle x1="1.655" y1="5.215" x2="11.945" y2="5.225" layer="21"/>
<rectangle x1="0.205" y1="5.225" x2="1.255" y2="5.235" layer="21"/>
<rectangle x1="1.655" y1="5.225" x2="11.965" y2="5.235" layer="21"/>
<rectangle x1="0.205" y1="5.235" x2="1.265" y2="5.245" layer="21"/>
<rectangle x1="1.655" y1="5.235" x2="11.975" y2="5.245" layer="21"/>
<rectangle x1="0.195" y1="5.245" x2="1.285" y2="5.255" layer="21"/>
<rectangle x1="1.655" y1="5.245" x2="11.985" y2="5.255" layer="21"/>
<rectangle x1="0.195" y1="5.255" x2="1.295" y2="5.265" layer="21"/>
<rectangle x1="1.655" y1="5.255" x2="11.995" y2="5.265" layer="21"/>
<rectangle x1="0.195" y1="5.265" x2="1.305" y2="5.275" layer="21"/>
<rectangle x1="1.655" y1="5.265" x2="12.005" y2="5.275" layer="21"/>
<rectangle x1="0.195" y1="5.275" x2="1.325" y2="5.285" layer="21"/>
<rectangle x1="1.655" y1="5.275" x2="12.015" y2="5.285" layer="21"/>
<rectangle x1="0.185" y1="5.285" x2="1.335" y2="5.295" layer="21"/>
<rectangle x1="1.655" y1="5.285" x2="12.025" y2="5.295" layer="21"/>
<rectangle x1="0.185" y1="5.295" x2="1.345" y2="5.305" layer="21"/>
<rectangle x1="1.655" y1="5.295" x2="12.035" y2="5.305" layer="21"/>
<rectangle x1="0.175" y1="5.305" x2="1.355" y2="5.315" layer="21"/>
<rectangle x1="1.655" y1="5.305" x2="12.045" y2="5.315" layer="21"/>
<rectangle x1="0.175" y1="5.315" x2="1.375" y2="5.325" layer="21"/>
<rectangle x1="1.655" y1="5.315" x2="12.055" y2="5.325" layer="21"/>
<rectangle x1="0.165" y1="5.325" x2="1.385" y2="5.335" layer="21"/>
<rectangle x1="1.655" y1="5.325" x2="12.065" y2="5.335" layer="21"/>
<rectangle x1="0.155" y1="5.335" x2="1.395" y2="5.345" layer="21"/>
<rectangle x1="1.655" y1="5.335" x2="12.075" y2="5.345" layer="21"/>
<rectangle x1="0.155" y1="5.345" x2="1.415" y2="5.355" layer="21"/>
<rectangle x1="1.655" y1="5.345" x2="12.085" y2="5.355" layer="21"/>
<rectangle x1="0.145" y1="5.355" x2="1.425" y2="5.365" layer="21"/>
<rectangle x1="1.655" y1="5.355" x2="12.095" y2="5.365" layer="21"/>
<rectangle x1="0.135" y1="5.365" x2="1.435" y2="5.375" layer="21"/>
<rectangle x1="1.655" y1="5.365" x2="12.105" y2="5.375" layer="21"/>
<rectangle x1="0.125" y1="5.375" x2="1.455" y2="5.385" layer="21"/>
<rectangle x1="1.655" y1="5.375" x2="12.115" y2="5.385" layer="21"/>
<rectangle x1="0.115" y1="5.385" x2="1.465" y2="5.395" layer="21"/>
<rectangle x1="1.655" y1="5.385" x2="12.125" y2="5.395" layer="21"/>
<rectangle x1="0.115" y1="5.395" x2="1.475" y2="5.405" layer="21"/>
<rectangle x1="1.655" y1="5.395" x2="12.135" y2="5.405" layer="21"/>
<rectangle x1="0.105" y1="5.405" x2="1.495" y2="5.415" layer="21"/>
<rectangle x1="1.655" y1="5.405" x2="12.145" y2="5.415" layer="21"/>
<rectangle x1="0.095" y1="5.415" x2="1.505" y2="5.425" layer="21"/>
<rectangle x1="1.655" y1="5.415" x2="12.155" y2="5.425" layer="21"/>
<rectangle x1="0.085" y1="5.425" x2="1.515" y2="5.435" layer="21"/>
<rectangle x1="1.655" y1="5.425" x2="12.165" y2="5.435" layer="21"/>
<rectangle x1="0.075" y1="5.435" x2="1.525" y2="5.445" layer="21"/>
<rectangle x1="1.655" y1="5.435" x2="12.175" y2="5.445" layer="21"/>
<rectangle x1="0.065" y1="5.445" x2="1.535" y2="5.455" layer="21"/>
<rectangle x1="1.655" y1="5.445" x2="12.185" y2="5.455" layer="21"/>
<rectangle x1="0.055" y1="5.455" x2="1.535" y2="5.465" layer="21"/>
<rectangle x1="1.655" y1="5.455" x2="12.195" y2="5.465" layer="21"/>
<rectangle x1="0.045" y1="5.465" x2="1.475" y2="5.475" layer="21"/>
<rectangle x1="1.655" y1="5.465" x2="12.205" y2="5.475" layer="21"/>
<rectangle x1="0.045" y1="5.475" x2="1.245" y2="5.485" layer="21"/>
<rectangle x1="1.655" y1="5.475" x2="12.215" y2="5.485" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="LOGO">
<text x="0" y="0" size="1.6764" layer="94">LOGO</text>
</symbol>
<symbol name="V_BATT">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="V_BATT" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="A5L-LOC">
<wire x1="85.09" y1="3.81" x2="85.09" y2="24.13" width="0.1016" layer="94"/>
<wire x1="85.09" y1="24.13" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<wire x1="139.065" y1="24.13" x2="180.34" y2="24.13" width="0.1016" layer="94"/>
<wire x1="170.18" y1="3.81" x2="170.18" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="180.34" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="139.065" y2="8.89" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="3.81" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="180.34" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="139.065" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="180.34" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<text x="140.97" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="140.97" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="154.305" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="140.716" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="184.15" y2="133.35" columns="4" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO">
<gates>
<gate name="G$1" symbol="LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LOGO_ITS">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V_BATT" prefix="SUPPLY">
<description>&lt;b&gt;V_BATT&lt;/b&gt;&lt;br&gt;
Generic symbol for the battery input to a system.</description>
<gates>
<gate name="G$1" symbol="V_BATT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A5L-LOC" prefix="FRAME" uservalue="yes">
<description>A5L LOC</description>
<gates>
<gate name="G$1" symbol="A5L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="holes">
<description>&lt;b&gt;Mounting Holes and Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,8-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 2.8 mm, round</description>
<wire x1="0" y1="2.921" x2="0" y2="2.667" width="0.0508" layer="21"/>
<wire x1="0" y1="-2.667" x2="0" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="0" x2="0" y2="-1.778" width="2.286" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="1.778" x2="1.778" y2="0" width="2.286" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.635" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="39"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="40"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="43"/>
<circle x="0" y="0" radius="1.5" width="0.2032" layer="21"/>
<pad name="B2,8" x="0" y="0" drill="2.8" diameter="5.334"/>
</package>
<package name="3,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.0 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="39"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.6" width="0.2032" layer="21"/>
<pad name="B3,0" x="0" y="0" drill="3" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="3,2-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="3,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.3 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3" x="0" y="0" drill="3.3" diameter="5.842"/>
</package>
<package name="3,6-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.6 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.9" width="0.2032" layer="21"/>
<pad name="B3,6" x="0" y="0" drill="3.6" diameter="5.842"/>
</package>
<package name="4,1-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.1 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="5.08" width="2" layer="43"/>
<circle x="0" y="0" radius="2.15" width="0.2032" layer="21"/>
<pad name="B4,1" x="0" y="0" drill="4.1" diameter="8"/>
</package>
<package name="4,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.3 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.25" width="0.1524" layer="21"/>
<pad name="B4,3" x="0" y="0" drill="4.3" diameter="9"/>
</package>
<package name="4,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.35" width="0.1524" layer="21"/>
<pad name="B4,5" x="0" y="0" drill="4.5" diameter="9"/>
</package>
<package name="5,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.0 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.6" width="0.1524" layer="21"/>
<pad name="B5" x="0" y="0" drill="5" diameter="9"/>
</package>
<package name="5,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.85" width="0.1524" layer="21"/>
<pad name="B5,5" x="0" y="0" drill="5.5" diameter="9"/>
</package>
</packages>
<symbols>
<symbol name="MOUNT-PAD">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOUNT-PAD-ROUND" prefix="H">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt;, round</description>
<gates>
<gate name="G$1" symbol="MOUNT-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="2.8" package="2,8-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.0" package="3,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.2" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3" package="3,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.6" package="3,6-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.1" package="4,1-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.3" package="4,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.5" package="4,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.0" package="5,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.5" package="5,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#afb-Electromechanicals">
<packages>
<package name="SWITCH_DPDT_TINY">
<pad name="3" x="-2.54" y="-2.54" drill="0.8"/>
<pad name="2" x="-2.54" y="0" drill="0.8"/>
<pad name="1" x="-2.54" y="2.54" drill="0.8" shape="square"/>
<pad name="6" x="2.54" y="2.54" drill="0.8"/>
<pad name="5" x="2.54" y="0" drill="0.8"/>
<pad name="4" x="2.54" y="-2.54" drill="0.8"/>
<wire x1="-4.25" y1="4.25" x2="4.25" y2="4.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="4.25" x2="4.25" y2="-4.25" width="0.127" layer="21"/>
<wire x1="4.25" y1="-4.25" x2="-4.25" y2="-4.25" width="0.127" layer="21"/>
<wire x1="-4.25" y1="-4.25" x2="-4.25" y2="4.25" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.9525" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0.9525" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.9525" x2="0.635" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.9525" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="0.635" x2="-0.9525" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.27" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.9525" x2="0.635" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.9525" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0.9525" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-1.27" x2="0.9525" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.9525" y1="0.635" x2="1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.3175" x2="0.635" y2="0.3175" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.3175" x2="0.635" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.3175" x2="-0.635" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.3175" x2="-0.635" y2="0.3175" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.27" y="0.635"/>
<vertex x="-0.9525" y="0.635"/>
<vertex x="-0.9525" y="-1.27"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="0.9525" y="0.635"/>
<vertex x="1.27" y="0.635"/>
<vertex x="1.27" y="-1.27"/>
<vertex x="0.9525" y="-1.27"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-0.635" y="-1.27"/>
<vertex x="0.635" y="-1.27"/>
<vertex x="0.635" y="-0.9525"/>
<vertex x="-0.635" y="-0.9525"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-0.635" y="1.27"/>
<vertex x="-0.635" y="0.9525"/>
<vertex x="0.635" y="0.9525"/>
<vertex x="0.635" y="1.27"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="SWITCH_DPDT">
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0" x2="-1.905" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<circle x="-6.35" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-7.62" y="-2.54" size="1.778" layer="95" rot="R270">&gt;PART</text>
<pin name="O0" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S0" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="P0" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<wire x1="10.16" y1="-3.175" x2="10.16" y2="-1.905" width="0.254" layer="94"/>
<wire x1="10.16" y1="3.175" x2="10.16" y2="1.905" width="0.254" layer="94"/>
<wire x1="6.35" y1="0" x2="10.795" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<circle x="6.35" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R270">&gt;PART</text>
<pin name="O1" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S1" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="P1" x="2.54" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SWITCH_DPDT" prefix="SW" uservalue="yes">
<gates>
<gate name="G$1" symbol="SWITCH_DPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH_DPDT_TINY">
<connects>
<connect gate="G$1" pin="O0" pad="3"/>
<connect gate="G$1" pin="O1" pad="4"/>
<connect gate="G$1" pin="P0" pad="2"/>
<connect gate="G$1" pin="P1" pad="5"/>
<connect gate="G$1" pin="S0" pad="1"/>
<connect gate="G$1" pin="S1" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U2" library="#afb-Boards" deviceset="INA219-MODULE" device=""/>
<part name="U1" library="#afb-Boards" deviceset="LOLIND32" device="" value="LOLIN D32 Pro"/>
<part name="V1" library="SparkFun-Connectors" deviceset="BANANA_CONN" device=""/>
<part name="V2" library="SparkFun-Connectors" deviceset="BANANA_CONN" device=""/>
<part name="A1" library="SparkFun-Connectors" deviceset="BANANA_CONN" device=""/>
<part name="A2" library="SparkFun-Connectors" deviceset="BANANA_CONN" device=""/>
<part name="+3V1" library="#afb-Aesthetics" deviceset="+3V3" device=""/>
<part name="GND2" library="#afb-Aesthetics" deviceset="GND" device=""/>
<part name="+3V2" library="#afb-Aesthetics" deviceset="+3V3" device=""/>
<part name="GND3" library="#afb-Aesthetics" deviceset="GND" device=""/>
<part name="GND4" library="#afb-Aesthetics" deviceset="GND" device=""/>
<part name="GND5" library="#afb-Aesthetics" deviceset="GND" device=""/>
<part name="BAT1" library="#afb-Boards" deviceset="BATT_18650_HOLDER" device=""/>
<part name="H1" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.8"/>
<part name="H2" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.8"/>
<part name="H3" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.8"/>
<part name="H4" library="holes" deviceset="MOUNT-PAD-ROUND" device="2.8"/>
<part name="SW1" library="#afb-Electromechanicals" deviceset="SWITCH_DPDT" device="" value="DPDT"/>
<part name="U$1" library="#afb-Aesthetics" deviceset="LOGO" device=""/>
<part name="U$2" library="#afb-Aesthetics" deviceset="LOGO" device=""/>
<part name="SUPPLY1" library="#afb-Aesthetics" deviceset="V_BATT" device=""/>
<part name="FRAME1" library="#afb-Aesthetics" deviceset="A5L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U2" gate="G$1" x="83.82" y="86.36"/>
<instance part="U1" gate="G$1" x="134.62" y="83.82"/>
<instance part="V1" gate="G$1" x="38.1" y="93.98" rot="R180"/>
<instance part="V2" gate="G$1" x="38.1" y="101.6" rot="R180"/>
<instance part="A1" gate="G$1" x="38.1" y="76.2" rot="R180"/>
<instance part="A2" gate="G$1" x="38.1" y="83.82" rot="R180"/>
<instance part="+3V1" gate="G$1" x="109.22" y="109.22"/>
<instance part="GND2" gate="1" x="73.66" y="45.72"/>
<instance part="+3V2" gate="G$1" x="68.58" y="101.6"/>
<instance part="GND3" gate="1" x="50.8" y="96.52"/>
<instance part="GND4" gate="1" x="60.96" y="91.44" rot="R270"/>
<instance part="GND5" gate="1" x="170.18" y="93.98"/>
<instance part="BAT1" gate="P" x="73.66" y="55.88"/>
<instance part="H1" gate="G$1" x="35.56" y="10.16"/>
<instance part="H2" gate="G$1" x="35.56" y="15.24"/>
<instance part="H3" gate="G$1" x="35.56" y="20.32"/>
<instance part="H4" gate="G$1" x="35.56" y="25.4"/>
<instance part="SW1" gate="G$1" x="96.52" y="60.96" rot="R270"/>
<instance part="U$1" gate="G$1" x="20.32" y="20.32"/>
<instance part="U$2" gate="G$1" x="20.32" y="22.86"/>
<instance part="SUPPLY1" gate="G$1" x="104.14" y="68.58"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VIN-"/>
<wire x1="71.12" y1="83.82" x2="40.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="40.64" y1="101.6" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
<wire x1="50.8" y1="101.6" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="71.12" y1="91.44" x2="63.5" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="154.94" y1="101.6" x2="170.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="170.18" y1="101.6" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BAT1" gate="P" pin="N"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="109.22" y1="106.68" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="3V3"/>
<wire x1="109.22" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="71.12" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="68.58" y1="93.98" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="V1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<wire x1="45.72" y1="85.09" x2="45.72" y2="93.98" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="1"/>
<wire x1="40.64" y1="76.2" x2="45.72" y2="76.2" width="0.1524" layer="91"/>
<wire x1="45.72" y1="76.2" x2="68.58" y2="76.2" width="0.1524" layer="91"/>
<wire x1="68.58" y1="76.2" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VIN+"/>
<wire x1="68.58" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="45.72" y1="76.2" x2="45.72" y2="82.55" width="0.1524" layer="91"/>
<junction x="45.72" y="76.2"/>
<wire x1="45.72" y1="82.55" x2="45.72" y2="85.09" width="0.1524" layer="91" curve="180"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="71.12" y1="88.9" x2="63.5" y2="88.9" width="0.1524" layer="91"/>
<label x="63.5" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GPIO22/SCL"/>
<wire x1="154.94" y1="96.52" x2="160.02" y2="96.52" width="0.1524" layer="91"/>
<label x="157.48" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="71.12" y1="86.36" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<label x="63.5" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GPIO21/SDA"/>
<wire x1="154.94" y1="88.9" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<label x="157.48" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="BAT1" gate="P" pin="P"/>
<wire x1="73.66" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<wire x1="88.9" y1="60.96" x2="88.9" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P0"/>
<wire x1="88.9" y1="71.12" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<wire x1="88.9" y1="60.96" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<junction x="88.9" y="60.96"/>
<pinref part="SW1" gate="G$1" pin="P1"/>
<wire x1="88.9" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V_BATT" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="S0"/>
<wire x1="101.6" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="S1"/>
<wire x1="104.14" y1="63.5" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<wire x1="104.14" y1="50.8" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="BAT"/>
<wire x1="114.3" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<junction x="104.14" y="63.5"/>
<pinref part="SUPPLY1" gate="G$1" pin="V_BATT"/>
<wire x1="104.14" y1="68.58" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
